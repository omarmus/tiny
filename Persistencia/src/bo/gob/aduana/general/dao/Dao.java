package bo.gob.aduana.general.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import java.text.Normalizer;

import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;


public class Dao {
    
    public static String JDBC = "";

    public Connection conexion() throws ClassNotFoundException, SQLException, NamingException {

        Connection cn = null;
        Statement st = null;
        
        InitialContext ic = new InitialContext();
        
        DataSource ds = (DataSource)ic.lookup(JDBC);
        cn = ds.getConnection();
        st = cn.createStatement();
        return cn;
    }
    
    // Filtro para quitar acentos, eñes, carácters extraños para guardar en SIDUNEA
    public String normalize(String input) {
        // Descomposición canónica
        String normalized = Normalizer.normalize(input, Normalizer.Form.NFD);
        // Nos quedamos únicamente con los caracteres ASCII
        Pattern pattern = Pattern.compile("\\P{ASCII}+");
        return pattern.matcher(normalized).replaceAll("");
    }
}
