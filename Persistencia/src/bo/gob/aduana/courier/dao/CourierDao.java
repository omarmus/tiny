package bo.gob.aduana.courier.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import bo.gob.aduana.courier.ent.Courier;

import bo.gob.aduana.courier.ent.Descargo;

import bo.gob.aduana.general.dao.Dao;

import java.io.IOException;

import java.sql.CallableStatement;
import java.sql.Connection;

import java.util.List;

import javax.naming.NamingException;

import oracle.jdbc.OracleTypes;

/**
 *
 * @author Omar Gutiérrez Condori <ogutierrez@aduana.gob.bo>
 */

public class CourierDao extends Dao{
    
    public Courier getCourier (String fecha_partida, String aduana, String nro_viaje, String doc_embarque) throws SQLException, ClassNotFoundException, NamingException {
        
        Connection con = null;
        CallableStatement call = null;
        ResultSet rs = null;
        Courier courier = null;

        try {
            con = conexion();
            call = con.prepareCall("{ ? = call pkg_courier.f_obtener_courier ( ?, ?, ?, ? )}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setString(2, fecha_partida);
            call.setString(3, aduana);
            call.setString(4, nro_viaje);
            call.setString(5, doc_embarque);
            call.execute();
                
            rs = (ResultSet) call.getObject(1);

            if (rs.next()) {
                courier = new Courier();
                courier.setAduana(rs.getString("key_cuo"));
                courier.setAnio_manif(rs.getString("car_reg_year"));
                courier.setNro_manif(rs.getInt("car_reg_nber"));
                courier.setFecha_manif(rs.getString("car_reg_date"));
                courier.setNro_viaje(rs.getString("key_voy_nber"));
                courier.setFecha_partida(rs.getString("key_dep_date"));
                courier.setDoc_embarque(rs.getString("key_bol_ref"));
                courier.setNro_linea(rs.getString("key_lin_nbr"));
                courier.setDoc_consignatario(rs.getString("cou_doc_consig"));
                courier.setConsignatario(rs.getString("cou_consignatario"));
                courier.setPeso(rs.getDouble("cou_peso_manif"));
                courier.setCantidad(rs.getInt("cou_cantidad"));
                courier.setPeso_real(rs.getDouble("cou_peso_real"));
                courier.setPeso_observado(rs.getDouble("cou_peso_obs"));
                courier.setTipo(rs.getString("cou_tipo_guia"));
                courier.setObservacion(rs.getString("cou_observacion"));
                courier.setDescripcion(rs.getString("cou_descripcion"));
                courier.setEstado(rs.getString("cou_estado"));
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return courier;
    }
    
    public Courier getGuiaImpresion (String gestion, String aduana, String manifiesto, String doc_embarque) throws SQLException, ClassNotFoundException, NamingException {
        
        Connection con = null;
        CallableStatement call = null;
        ResultSet rs = null;
        Courier courier = null;

        try {
            con = conexion();
            call = con.prepareCall("{ ? = call pkg_courier.f_obtener_guia_impresion ( ?, ?, ?, ? )}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setString(2, gestion);
            call.setString(3, aduana);
            call.setString(4, manifiesto);
            call.setString(5, doc_embarque);
            call.execute();
                
            rs = (ResultSet) call.getObject(1);

            if (rs.next()) {
                courier = new Courier();
                courier.setAduana(rs.getString("key_cuo"));
                courier.setAnio_manif(rs.getString("car_reg_year"));
                courier.setNro_manif(rs.getInt("car_reg_nber"));
                courier.setFecha_manif(rs.getString("car_reg_date"));
                courier.setNro_viaje(rs.getString("key_voy_nber"));
                courier.setFecha_partida(rs.getString("key_dep_date"));
                courier.setDoc_embarque(rs.getString("key_bol_ref"));
                courier.setNro_linea(rs.getString("key_lin_nbr"));
                courier.setDoc_consignatario(rs.getString("cou_doc_consig"));
                courier.setConsignatario(rs.getString("cou_consignatario"));
                courier.setPeso(rs.getDouble("cou_peso_manif"));
                courier.setCantidad(rs.getInt("cou_cantidad"));
                courier.setPeso_real(rs.getDouble("cou_peso_real"));
                courier.setPeso_observado(rs.getDouble("cou_peso_obs"));
                courier.setTipo(rs.getString("cou_tipo_guia"));
                courier.setObservacion(rs.getString("cou_observacion"));
                courier.setDescripcion(rs.getString("cou_descripcion"));
                courier.setEstado(rs.getString("cou_estado"));
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return courier;
    }
           
    public List<Courier> lista(String aduana) throws SQLException, ClassNotFoundException, NamingException {
        
        Connection con = null;
        CallableStatement call = null;
        ResultSet rs = null;
        
        List<Courier> couriers = new ArrayList<Courier>();

        try {
            con = conexion();
            call = con.prepareCall("{ ? = call pkg_courier.f_lista ( ? )}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setString(2, aduana);
            call.execute();
                
            rs = (ResultSet) call.getObject(1);

            while (rs.next()) {
                Courier courier = new Courier();
                courier.setAduana(rs.getString("key_cuo"));
                courier.setAnio_manif(rs.getString("car_reg_year"));
                courier.setNro_manif(rs.getInt("car_reg_nber"));
                courier.setFecha_manif(rs.getString("car_reg_date"));
                courier.setNro_viaje(rs.getString("key_voy_nber"));
                courier.setFecha_partida(rs.getString("key_dep_date"));
                courier.setDoc_embarque(rs.getString("key_bol_ref"));
                courier.setNro_linea(rs.getString("key_lin_nbr"));
                courier.setDoc_consignatario(rs.getString("cou_doc_consig"));
                courier.setConsignatario(rs.getString("cou_consignatario"));
                courier.setPeso(rs.getDouble("cou_peso_manif"));
                courier.setCantidad(rs.getInt("cou_cantidad"));
                courier.setPeso_real(rs.getDouble("cou_peso_real"));
                courier.setPeso_observado(rs.getDouble("cou_peso_obs"));
                courier.setTipo(rs.getString("cou_tipo_guia"));
                courier.setObservacion(rs.getString("cou_observacion"));
                courier.setDescripcion(rs.getString("cou_descripcion"));
                courier.setEstado(rs.getString("cou_estado"));
                couriers.add(courier);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return couriers;
    }
    
    public List<Descargo> getDescargos(String fecha_partida, String aduana, String nro_viaje, String doc_embarque) throws SQLException, ClassNotFoundException, NamingException {
               
        Connection con = null;
        CallableStatement call = null;
        ResultSet rs = null;
        
        List<Descargo> descargos = new ArrayList<Descargo>();

        try {
            con = conexion();
            call = con.prepareCall("{ ? = call pkg_courier.f_descargos ( ?, ?, ?, ? )}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setString(2, fecha_partida);
            call.setString(3, aduana);
            call.setString(4, nro_viaje);
            call.setString(5, doc_embarque);
            call.execute();
                
            rs = (ResultSet) call.getObject(1);

            while (rs.next()) {
                Descargo descargo = new Descargo();
                descargo.setKey_cuo(rs.getString("key_cuo"));
                descargo.setKey_voy_nber(rs.getString("key_voy_nber"));
                descargo.setKey_dep_date(rs.getString("key_dep_date"));
                descargo.setKey_bol_ref(rs.getString("key_bol_ref"));
                descargo.setReg_cuo(rs.getString("reg_cuo"));
                descargo.setReg_voy_nber(rs.getString("reg_voy_nber"));
                descargo.setReg_dep_date(rs.getString("reg_dep_date"));
                descargo.setReg_bol_ref(rs.getString("reg_bol_ref"));
                descargo.setReg_peso(rs.getDouble("reg_peso"));
                descargos.add(descargo);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return descargos;
    }
    
    public List<Courier> reportePendientes(String aduana, String nro_manif, String doc_embarque, String fecha_manif, String usuario, String tipo)  throws SQLException, ClassNotFoundException, NamingException {
        
        Connection con = null;
        CallableStatement call = null;
        ResultSet rs = null;
        List<Courier> couriers = new ArrayList<Courier>();

        try {
            con = conexion();
            call = con.prepareCall("{ ? = call pkg_courier.f_reporte_pendientes ( ?, ?, ?, ?, ?, ? )}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setString(2, aduana);
            call.setString(3, nro_manif);
            call.setString(4, doc_embarque); 
            call.setString(5, fecha_manif);
            call.setString(6, usuario);
            call.setString(7, tipo);
            call.execute();
                
            rs = (ResultSet) call.getObject(1);

            while (rs.next()) {
                Courier courier = new Courier();
                courier.setAduana(rs.getString("key_cuo"));
                courier.setAnio_manif(rs.getString("car_reg_year"));
                courier.setNro_manif(rs.getInt("car_reg_nber"));
                courier.setFecha_manif(rs.getString("car_reg_date"));
                courier.setNro_viaje(rs.getString("key_voy_nber"));
                courier.setFecha_partida(rs.getString("key_dep_date"));
                courier.setDoc_embarque(rs.getString("key_bol_ref"));
                courier.setNro_linea(rs.getString("key_lin_nbr"));
                courier.setDoc_consignatario(rs.getString("cou_doc_consig"));
                courier.setConsignatario(rs.getString("cou_consignatario"));
                courier.setPeso(rs.getDouble("cou_peso_manif"));
                courier.setCantidad(rs.getInt("cou_cantidad"));
                courier.setPeso_real(rs.getDouble("cou_peso_real"));
                courier.setPeso_observado(rs.getDouble("cou_peso_obs"));
                courier.setTipo(rs.getString("cou_tipo_guia"));
                courier.setObservacion(rs.getString("cou_observacion"));
                courier.setDescripcion(rs.getString("cou_descripcion"));
                courier.setEstado(rs.getString("cou_estado"));
                couriers.add(courier);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return couriers;
    }
    
    public List<Courier> reporte(String aduana, String nro_manif, String doc_embarque, String fecha_manif, String usuario, String tipo, String estado) throws SQLException, ClassNotFoundException, NamingException {
        
        Connection con = null;
        CallableStatement call = null;
        ResultSet rs = null;
        List<Courier> couriers = new ArrayList<Courier>();

        try {
            con = conexion();
            call = con.prepareCall("{ ? = call pkg_courier.f_reporte ( ?, ?, ?, ?, ?, ?, ? )}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setString(2, aduana);
            call.setString(3, nro_manif);
            call.setString(4, doc_embarque); 
            call.setString(5, fecha_manif);
            call.setString(6, usuario);
            call.setString(7, tipo);
            call.setString(8, estado);
            call.execute();
                
            rs = (ResultSet) call.getObject(1);

            while (rs.next()) {
                Courier courier = new Courier();
                courier.setAduana(rs.getString("key_cuo"));
                courier.setAnio_manif(rs.getString("car_reg_year"));
                courier.setNro_manif(rs.getInt("car_reg_nber"));
                courier.setFecha_manif(rs.getString("car_reg_date"));
                courier.setNro_viaje(rs.getString("key_voy_nber"));
                courier.setFecha_partida(rs.getString("key_dep_date"));
                courier.setDoc_embarque(rs.getString("key_bol_ref"));
                courier.setNro_linea(rs.getString("key_lin_nbr"));
                courier.setDoc_consignatario(rs.getString("cou_doc_consig"));
                courier.setConsignatario(rs.getString("cou_consignatario"));
                courier.setPeso(rs.getDouble("cou_peso_manif"));
                courier.setCantidad(rs.getInt("cou_cantidad"));
                courier.setPeso_real(rs.getDouble("cou_peso_real"));
                courier.setPeso_observado(rs.getDouble("cou_peso_obs"));
                courier.setTipo(rs.getString("cou_tipo_guia"));
                courier.setObservacion(rs.getString("cou_observacion"));
                courier.setDescripcion(rs.getString("cou_descripcion"));
                courier.setEstado(rs.getString("cou_estado"));
                couriers.add(courier);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return couriers;
    }
    
    public String guardar(Courier courier) throws SQLException, ClassNotFoundException, NamingException {
        
        Connection con = null;
        CallableStatement call = null;
        String res = "";

        try {
            con = conexion();
            call = con.prepareCall("{ ? = call pkg_courier.f_guardar_courier ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )}");
            call.registerOutParameter(1, OracleTypes.VARCHAR);
            call.setString(2, courier.getAduana());
            call.setString(3, courier.getNro_viaje());
            call.setString(4, courier.getFecha_partida());
            call.setString(5, courier.getDoc_embarque());
            call.setString(6, courier.getNro_linea());
            call.setString(7, courier.getAnio_manif());
            call.setInt(8, courier.getNro_manif());
            call.setString(9, courier.getFecha_manif());
            call.setString(10, courier.getDoc_consignatario());
            call.setString(11, courier.getConsignatario());
            call.setString(12, courier.getTipo());
            call.setInt(13, courier.getCantidad());
            call.setDouble(14, courier.getPeso());
            call.setDouble(15, courier.getPeso_real());
            call.setDouble(16, courier.getPeso_observado());
            call.setString(17, normalize(courier.getObservacion().toUpperCase()));
            call.setString(18, courier.getDescripcion());
            call.setString(19, courier.getDes_gestion());
            call.setString(20, courier.getDes_aduana());
            call.setString(21, courier.getDes_manifiesto());
            call.setString(22, courier.getDocumentos());
            call.setString(23, courier.getEstado());            
            call.setString(24, courier.getUsuario());
            call.execute();
                
            res = call.getString(1);

        } finally {
            if (con != null) {
                con.close();
            }
        }
        return res;
    }
       
    public List<Courier> guias(String query, String gestion, String aduana, String manifiesto) throws SQLException, ClassNotFoundException, NamingException {
        
        Connection con = null;
        CallableStatement call = null;
        ResultSet rs = null;
        List<Courier> couriers = new ArrayList<Courier>();

        try {
            con = conexion();
            call = con.prepareCall("{ ? = call pkg_courier.f_lista_guias ( ?, ?, ?, ? )}");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setString(2, query);
            call.setString(3, gestion);
            call.setString(4, aduana);
            call.setString(5, manifiesto);
            call.execute();
                
            rs = (ResultSet) call.getObject(1);

            while (rs.next()) {
                Courier courier = new Courier();
                courier.setDoc_embarque(rs.getString("key_bol_ref"));
                courier.setPeso(rs.getDouble("car_wgt_avl"));
                couriers.add(courier);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return couriers;
    }
    
    public String pesoReal (Courier courier) throws SQLException, ClassNotFoundException, NamingException {
        
        Connection con = null;
        CallableStatement call = null;
        String res = "";

        try {
            con = conexion();
            call = con.prepareCall("{ ? = call pkg_courier.f_guia_peso_real ( ?, ?, ?, ? )}");
            call.registerOutParameter(1, OracleTypes.VARCHAR);
            call.setString(2, courier.getFecha_partida());
            call.setString(3, courier.getAduana());
            call.setString(4, courier.getNro_viaje());
            call.setString(5, courier.getDoc_embarque());
            call.execute();
  
            res = call.getString(1);

        } finally {
            if (con != null) {
                con.close();
            }
        }
        return res;
    }
}
