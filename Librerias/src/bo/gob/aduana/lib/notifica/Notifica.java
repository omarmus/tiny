package bo.gob.aduana.lib.notifica;

/**
 *
 * @author Omar Gutiérrez Condori <ogutierrez@aduana.gob.bo>
 */


import com.google.gson.JsonObject;

import java.security.InvalidKeyException;
import java.security.SignatureException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import java.util.Calendar;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;

import net.oauth.jsontoken.JsonToken;
import net.oauth.jsontoken.crypto.HmacSHA256Signer;


public class Notifica {

    private static final String AUDIENCE = "ANB";

    private static final String ISSUER = "Notifica-APP";

    private static final String SIGNING_KEY = "4dV4N4$n4c10n4l$d3$b0l1v14$n0t1f1C4$4pp$201S!";
    
    protected static String JDBC = "jdbc/notifica";
    
    /**
     * Creates a json web token which is a digitally signed token that contains a payload (e.g. userId to identify 
     * the user). The signing key is secret. That ensures that the token is authentic and has not been modified.
     * Using a jwt eliminates the need to store authentication session information in a database.
     * @param userId
     * @param durationDays
     * @return
     */
    public static String createJsonWebToken(String userId, Long durationDays)    {
        //Current time and signing algorithm
        Calendar cal = Calendar.getInstance();
        HmacSHA256Signer signer;
        try {
            signer = new HmacSHA256Signer(ISSUER, null, SIGNING_KEY.getBytes());
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        }

        //Configure JSON token
        JsonToken token = new net.oauth.jsontoken.JsonToken(signer);
        token.setAudience(AUDIENCE);
        token.setIssuedAt(new org.joda.time.Instant(cal.getTimeInMillis()));
        token.setExpiration(new org.joda.time.Instant(cal.getTimeInMillis() + 1000L * 60L * 60L * 24L * durationDays));
        
        //Configure request object, which provides information of the item
        JsonObject request = new JsonObject();
        request.addProperty("userId", userId);

        JsonObject payload = token.getPayloadAsJsonObject();
        payload.add("info", request);

        try {
            return token.serializeAndSign();
        } catch (SignatureException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static String saveToken (String user, String app, String ip, String userAgent) throws SQLException {
        
        Connection con = null;
        CallableStatement call = null;
        String respuesta = "";
        String token = "";
        try {
            token = Notifica.createJsonWebToken(user, (long) 10);
            con = Notifica.getConnection();
            call = con.prepareCall("{? = call pkg_mensajes.f_guarda_sesion(?,?,?,?,?)}");
            call.registerOutParameter(1, Types.VARCHAR);
            call.setString(2, token);
            call.setString(3, ip);
            call.setString(4, userAgent);
            call.setString(5, user);
            call.setString(6, app);
            call.execute();
            respuesta = call.getString(1);
            call.close();
        } catch (Exception e) {
            e.printStackTrace();
            if (call != null) {
                respuesta = call.getString(1);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        
        if (respuesta.equals("OK")) {
            return token;
        }
        System.err.println(respuesta);
        return "ERROR";
    }
        
    public static Connection getConnection() throws ClassNotFoundException, SQLException, NamingException {

        Connection cn = null;
        Statement st = null;
        
        InitialContext ic = new InitialContext();
        
        DataSource ds = (DataSource)ic.lookup(JDBC);
        cn = ds.getConnection();
        st = cn.createStatement();
        return cn;
    }

}