package bo.gob.aduana.lib.constantes;

public enum Estados {
    POR_REVISAR("POR_REVISAR"),
    RETENIDO("RETENIDO"),
    POR_AUTORIZAR("POR_AUTORIZAR"),
    CON_CONSTANCIA("CON_CONSTANCIA"),
    AUTORIZADO("AUTORIZADO");
    
    private String estado;
    
    private Estados(String estado){
        this.estado = estado;
    }
    
    public String value(){
        return this.estado;
    }
    
}
