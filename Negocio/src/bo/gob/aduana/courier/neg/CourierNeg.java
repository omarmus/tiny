package bo.gob.aduana.courier.neg;


import bo.gob.aduana.courier.dao.CourierDao;
import bo.gob.aduana.courier.ent.Courier;
import bo.gob.aduana.courier.ent.Descargo;
import bo.gob.aduana.general.ent.Respuesta;
import bo.gob.aduana.lib.log.Log;

import java.sql.SQLException;

import java.util.List;

import javax.naming.NamingException;


public class CourierNeg {
   
    private final CourierDao dao = new CourierDao();
    private final String CORRECTO_BD = "OK";
       
    private boolean estaConectadoBd() {
        return dao != null;
    }
    
    public Respuesta<Courier[]> lista(String aduana) {
        Respuesta<Courier[]> respuesta = new Respuesta<Courier[]>();
        respuesta.setCodigo(-1);
        if (estaConectadoBd()) {
            try {
                List<Courier> result = dao.lista(aduana);
                if (result == null || result.size() == 0) {
                    respuesta.setMensaje("No existen registros");
                } else {
                    respuesta.setCodigo(1);
                    respuesta.setMensaje("OK");
                    respuesta.setResultado(result.toArray(new Courier[result.size()]));
                }
            } catch (SQLException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (ClassNotFoundException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (NamingException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            }
        } else {
            respuesta.setMensaje("Error. No se puede conectar a la base de datos.");
        }

        return respuesta;
    }
    
    public Respuesta<Descargo[]> getDescargos(String fecha_partida, String aduana, String nro_viaje, String doc_embarque) {
        Respuesta<Descargo[]> respuesta = new Respuesta<Descargo[]>();
        respuesta.setCodigo(-1);
        if (estaConectadoBd()) {
            try {
                List<Descargo> result = dao.getDescargos(fecha_partida, aduana, nro_viaje, doc_embarque);
                if (result == null || result.size() == 0) {
                    respuesta.setMensaje("No existen registros");
                } else {
                    respuesta.setCodigo(1);
                    respuesta.setMensaje("OK");
                    respuesta.setResultado(result.toArray(new Descargo[result.size()]));
                }
            } catch (SQLException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (ClassNotFoundException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (NamingException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            }
        } else {
            respuesta.setMensaje("Error. No se puede conectar a la base de datos.");
        }

        return respuesta;
    }
    
    public Respuesta<Courier> getCourier(String fecha_partida, String aduana, String nro_viaje, String doc_embarque) {
        Respuesta<Courier> respuesta = new Respuesta<Courier>();
        respuesta.setCodigo(-1);
        if (estaConectadoBd()) {
            try {
                Courier result = dao.getCourier(fecha_partida, aduana, nro_viaje, doc_embarque);
                if (result == null) {
                    respuesta.setMensaje("No existe el registro");
                } else {
                    respuesta.setCodigo(1);
                    respuesta.setMensaje("OK");
                    respuesta.setResultado(result);
                }
            } catch (SQLException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (ClassNotFoundException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (NamingException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            }
        } else {
            respuesta.setMensaje("Error. No se puede conectar a la base de datos.");
        }

        return respuesta;
    }
    
    public Respuesta<Courier> getGuiaImpresion (String gestion, String aduana, String manifiesto, String doc_embarque) {
        Respuesta<Courier> respuesta = new Respuesta<Courier>();
        respuesta.setCodigo(-1);
        if (estaConectadoBd()) {
            try {
                Courier result = dao.getGuiaImpresion(gestion, aduana, manifiesto, doc_embarque);
                if (result == null) {
                    respuesta.setMensaje("No existe el registro");
                } else {
                    respuesta.setCodigo(1);
                    respuesta.setMensaje("OK");
                    respuesta.setResultado(result);
                }
            } catch (SQLException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (ClassNotFoundException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (NamingException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            }
        } else {
            respuesta.setMensaje("Error. No se puede conectar a la base de datos.");
        }

        return respuesta;
    }
    
    public Respuesta<Courier[]> reportePendientes(String aduana, String nro_manif, String doc_embarque, String fecha_manif, String usuario, String tipo) {
        Respuesta<Courier[]> respuesta = new Respuesta<Courier[]>();
        respuesta.setCodigo(-1);
        if (estaConectadoBd()) {
            try {
                List<Courier> result = dao.reportePendientes(aduana, nro_manif, doc_embarque, fecha_manif, usuario, tipo);
                if (result == null || result.size() == 0) {
                    respuesta.setMensaje("No existen registros");
                } else {
                    respuesta.setCodigo(1);
                    respuesta.setMensaje("OK");
                    respuesta.setResultado(result.toArray(new Courier[result.size()]));
                }
            } catch (SQLException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (ClassNotFoundException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (NamingException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            }
        } else {
            respuesta.setMensaje("Error. No se puede conectar a la base de datos.");
        }

        return respuesta;
    }
    
    public Respuesta<Courier[]> guias(String query, String gestion, String aduana, String manifiesto) {
        Respuesta<Courier[]> respuesta = new Respuesta<Courier[]>();
        respuesta.setCodigo(-1);
        if (estaConectadoBd()) {
            try {
                List<Courier> result = dao.guias(query, gestion, aduana, manifiesto);
                if (result == null || result.size() == 0) {
                    respuesta.setMensaje("No existen registros");
                } else {
                    respuesta.setCodigo(1);
                    respuesta.setMensaje("OK");
                    respuesta.setResultado(result.toArray(new Courier[result.size()]));
                }
            } catch (SQLException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (ClassNotFoundException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (NamingException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            }
        } else {
            respuesta.setMensaje("Error. No se puede conectar a la base de datos.");
        }

        return respuesta;
    }
    
    public Respuesta<Courier[]> reporte(String aduana, String nro_manif, String doc_embarque, String fecha_manif, String usuario, String tipo, String estado) {
        Respuesta<Courier[]> respuesta = new Respuesta<Courier[]>();
        respuesta.setCodigo(-1);
        if (estaConectadoBd()) {
            try {
                List<Courier> result = dao.reporte(aduana, nro_manif, doc_embarque, fecha_manif, usuario, tipo, estado);
                if (result == null || result.size() == 0) {
                    respuesta.setMensaje("No existen registros");
                } else {
                    respuesta.setCodigo(1);
                    respuesta.setMensaje("OK");
                    respuesta.setResultado(result.toArray(new Courier[result.size()]));
                }
            } catch (SQLException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (ClassNotFoundException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (NamingException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            }
        } else {
            respuesta.setMensaje("Error. No se puede conectar a la base de datos.");
        }

        return respuesta;
    }
    
    public Respuesta<Boolean> guardar (Courier courier) {
        Respuesta<Boolean> respuesta = new Respuesta<Boolean>();
        respuesta.setCodigo(-1);
        respuesta.setResultado(false);
        if (estaConectadoBd()) {
            try {
                String result = dao.guardar(courier);
                if (result == null) {
                    respuesta.setMensaje("No se pudo guardar la información");
                } else if (result.equals(CORRECTO_BD)) {
                    respuesta.setCodigo(1);
                    respuesta.setMensaje("OK");
                    respuesta.setResultado(true);
                } else {
                    respuesta.setMensaje(result);
                }
            } catch (SQLException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (ClassNotFoundException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (NamingException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            }
        } else {
            respuesta.setMensaje("Error. No se puede conectar a la base de datos.");
        }

        return respuesta;
    }
    
    public Respuesta<String> pesoReal (Courier courier) {
        Respuesta<String> respuesta = new Respuesta<String>();
        respuesta.setCodigo(-1);
        if (estaConectadoBd()) {
            try {
                String result = dao.pesoReal(courier);
                if (result == null) {
                    respuesta.setMensaje("No se pudo guardar la información");
                } else if (result.equals(CORRECTO_BD)) {
                    respuesta.setCodigo(1);
                    respuesta.setMensaje("OK");
                    respuesta.setResultado(result);
                } else {
                    respuesta.setMensaje(result);
                }
            } catch (SQLException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (ClassNotFoundException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            } catch (NamingException e) {
                respuesta.setMensaje("Error no identificado -  " + e.getMessage());
                Log.error("Error no identificado", "BASE DE DATOS", e);
            }
        } else {
            respuesta.setMensaje("Error. No se puede conectar a la base de datos.");
        }

        return respuesta;
    }

}
