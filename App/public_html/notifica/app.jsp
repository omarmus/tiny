<%@ page contentType="text/html;charset=utf-8"%>

<!DOCTYPE html>

<html lang="es">
    <head>
        <title>Notifica APP</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-Equiv="Cache-Control" Content="no-cache">
        <meta http-Equiv="Pragma" Content="no-cache">
        <meta http-Equiv="Expires" Content="0">
        <link rel="stylesheet" type="text/css" href="css/normalize.css">
        <link rel="stylesheet" type="text/css" href="css/app.css">
    </head>
    <body>
        <div class="not-container" id="container">
            <div class="not-header-main">
                <div class="not-loading"></div>
                <div class="not-header">
                    <h1>Notificaciones</h1>
                </div>            
                <div class="not-options">
                    <button type="button" class="btn btn-primary btn-xs" id="btn-add" style="display: none;">Nueva</button>
                    <button type="button" class="btn btn-default btn-xs hide" id="btn-read-all">Marcar todas como leidas</button>
                    <button type="button" class="btn btn-primary btn-xs btn-search" id="btn-search">Buscar</button>
                    <div class="not-notiweb"><button type="button" class="btn btn-default btn-xs" id="btn-notiweb">Habilitar notificaciones de escritorio</button></div>
                    <div class="search" id="container-search">
                        <input type="search" placeholder="Escriba su búsqueda y presione Enter">
                        <span title="Cerrar búsqueda">x</span>
                    </div>                    
                </div>
            </div>
            <form class="not-add" id="form-add">
                <div class="not-add-header">
                    <h3>Nueva notificación</h3>
                </div>
                <div class="not-field">                    
                    <label>Título: </label>
                    <input type="text" class="required" name="title" id="title" />
                    <div class="not-error">Campo requerido</div>
                </div>
                <div class="not-field">                    
                    <label>Mensaje: </label>
                    <textarea name="message" class="required"></textarea>
                    <div class="not-error">Campo requerido</div>
                </div>
                <div class="not-field">                    
                    <label>Destinatario: </label>
                    <input type="text" class="required" name="receiver" />
                    <div class="not-error">Campo requerido</div>
                </div>
                <div class="not-field">                    
                    <label>Tipo: </label>
                    <select name="type">
                        <option value="ALERT">ALERTA</option>
                        <option value="MESSAGE">MENSAJE</option>
                        <option value="INVITATION">INVITACIÓN</option>
                    </select>
                </div>
                <div class="not-add-footer text-right">
                    <button class="btn btn-default btn-sm" type="button" id="btn-cancel">Cancelar</button>
                    <button class="btn btn-primary btn-sm" type="submit">Enviar notificación</button>
                </div>
            </form>
            <div class="not-body-main">
                <div class="not-body">
                    <div id="not-items">
                        <div class="not-empty">No hay nuevas notificaciones.</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="not-alert" id="alert-modal"></div>
        <script type="text/tmpl-js" id="not-confirm-tmpl">
            <span class="not-close" title="Cerrar">x</span>
            <div class="not-alert-header">{title}</div>
            <div class="not-alert-body">{message}</div>
            <div class="not-alert-footer text-right">
                <button class="btn btn-default btn-sm {button_cancel}" type="button">Cancelar</button>
                <button class="btn btn-primary btn-sm" type="button">Aceptar</button>
            </div>
        </script>
        <script type="text/js-tmpl" id="not-item-tmpl">
            <div class="not-item {STATE}" data-id="{ID}" data-query="{QUERY}">
                <span class="not-close" title="Eliminar notificación">x</span>
                <div>
                    <div class="not-title">{TITLE}</div>
                    <div class="not-message">{MESSAGE}</div>
                    <div class="not-details">
                        <span class="left not-icon not-icon-{TYPE} "></span>
                        <span class="not-date left" data-date="{DATE}">{DATE_CREATED}</span>
                        <span class="not-sender right">
                            <em><strong>Remitente: </strong>{SENDER}</em>
                        </span>
                    </div>
                </div>
            </div>
        </script>
        <script type="text/js-tmpl" id="not-details-tmpl">
            <div class="not-details" id="not-details-{ID}" data-id="{ID}">
                <span class="not-close" title="Cerrar notificación">x</span>
                <div class="not-details-body">
                    <div>
                        <div class="not-details-title">
                            <strong>{TITLE}</strong>
                        </div>
                        <div class="not-details-message">{MESSAGE}</div>
                        <div class="not-details-details">
                            <span class="not-details-date not-left" title="{DATE_CREATED}">
                                <span class="not-left not-icon not-icon-{TYPE} "></span>
                                <em>El {DATE_CREATED}</em>
                            </span>
                            <span class="not-details-sender not-right">
                                <em><strong>Remitente: </strong>{SENDER}</em>
                            </span>
                        </div>
                    </div>
                </div>
                <span class="not-caret"></span>
            </div>
        </script>
        <script type="text/js-tmpl" id="not-alert-tmpl">
            <div class="not-alert" data-id="{ID}">
                <span class="not-close" title="Cerrar notificación">x</span>
                <div>
                    <div class="not-title">{TITLE}</div>
                    <div class="not-message">
                        {MESSAGE}
                    </div>
                    <div class="not-details-details">
                        <span class="not-left not-icon not-icon-{TYPE} "></span>
                        <span class="not-details-date not-left" title="{DATE_CREATED}">
                            <em>{DATE_CREATED}</em>
                        </span>
                        <span class="not-details-sender not-right">
                            <em><strong>Remitente: </strong>{SENDER}</em>
                        </span>
                    </div>
                </div>
            </div>
        </script>       
        <script type="text/js-tmpl" id="not-growl-tmpl">
            <div class="not-growl not-growl-{type}">
                <span class="not-close" title="Cerrar">x</span>
                <div class="not-growl-header">{title}</div>
                <div class="not-growl-body">{message}</div>
            </div>
        </script>
        <script>
            var APP = '${param.APP}';
            var USER = '${param.USER}';
            var TOKEN = '${param.TOKEN}';
        </script>
        <script src="js/jquery-1.11.2.min.js"></script>
        <script src="lib/socket.io/socket.io.js"></script>
        <script src="js/app.js"></script>
    </body>
</html>
