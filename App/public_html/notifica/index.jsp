<link rel="stylesheet" type="text/css" href="notifica/css/styles.css" />

<div id="notifica-app" class="not-app">
    <div class="not-open">
        <i class="arrow-open"></i>
        <span class="not-number" id="not-number-state--disabled"></span>
    </div>
    <div class="not-loading">Cargando aplicaci&oacute;n</div>
    <iframe src="notifica/app.jsp?APP=${sessionScope.APP}&USER=${sessionScope.USER}&TOKEN=${sessionScope.TOKEN}" id="iframe-notifica" name="notifica"></iframe>
</div>
<div id="not-alerts" class="not-alerts"></div>
<div id="not-growls" class="not-growls"></div>

<script src="notifica/js/scripts.js"></script>
