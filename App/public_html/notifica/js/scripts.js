// Module Notifica
var Notifica = (function (notifica, $) {

    var app = null;
    
    var isOpen = function () {
        return $('#notifica-app').css('margin-right') === '0px';
    }
        
    var init = function () {
    	var $container = $('#notifica-app');
        var $total = $('#not-number-state');
        
        $container.children('iframe').load(function () {
            this.style.display = 'block';
            $container.find('.not-loading').hide();
            app = window.frames.notifica.window.App;
        });
        var $opens = $('.not-open');
        $opens.click(function (e) {
            e.preventDefault();
            var open = isOpen(),
                $el = $(this);
            $container.animate({marginRight : !open ? 0 : '-277px'});
            $total.html('').hide();
            if (!open && app) {
                app.message.viewAll();
            }
            $opens.toggleClass('not-open-close');
            var $arrow = $opens.find('i[class*=arrow]');
            if ($arrow.hasClass('arrow-open')) {
                $arrow.removeClass('arrow-open').addClass('arrow-close');
            } else {
                $arrow.removeClass('arrow-close').addClass('arrow-open');
            }
            $('.not-growl').hide();
        });
        
        $('body').click(function() {
            $('.not-details').hide();
        });
    }

    init();
    
    notifica.isOpen = isOpen;
    
    notifica.create = function (receiver, title, message, type) {
        if (typeof app === 'undefined') {
            app = window.frames.notifica.window.App;
        }
        if (app.empty(receiver)) {
            alert('Debe ingresar un remitente(receiver) para la notificación.');
            return false;
        }
        if (app.empty(title)) {
            alert('Debe ingresar el título(title) de la notificación.');
            return false;
        }
        if (app.empty(message)) {
            alert('Debe ingresar el mensaje(message) para la notificación.');
            return false;
        }
        if (type === 'ALERT' || type === 'MESSAGE' || type === 'INVITATION') {
            app.message.create({
                receiver: receiver,
                title: title,
                message: message,
                type: type
            });
        } else {
            alert('El tipo tiene que ser ALERT, MESSAGE o INVITATION.');
            return false;
        }
    }

    return notifica;

}(Notifica || {}, jQuery));