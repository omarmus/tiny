var SERVER = 'http://10.100.100.249:3000/';
var SERVER1 = 'http://10.100.100.249:3000/';
//var SERVER = 'http://localhost:8888/';
//var SERVER1 = 'http://localhost:8888/';
var SERVER2 = 'http://localhost:7101/anb-service/notifica/';

var App = (function (app, $) {
   
    var $container = null;
    var $parent = null;
    var tmpl_alert = $('#not-confirm-tmpl').html();
    var $alert_modal = $("#alert-modal");

    var nano = function (template, data) {
        return template.replace(/\{([\w\.]*)\}/g, function (str, key) {
            var keys = key.split("."), v = data[keys.shift()];
            for (var i = 0, l = keys.length;i < l;i++)
                v = v[keys[i]];
            return (typeof v !== "undefined" && v !== null) ? v : "";
        });
    }

    var isIE = function () {
        if (navigator.appName == 'Microsoft Internet Explorer') {
            var re = new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})");
            if (re.exec(navigator.userAgent) != null) {
                return parseFloat( RegExp.$1 );
            }
        }
        return -1;
    }
    
    var showAlert = function () {
        App.$container.addClass('panel');
        $alert_modal.animate({marginLeft : '2px'}, 300);
        App.$parent.find('.not-details').hide();
        $alert_modal.find('.btn').on('click', function () {
            App.$container.removeClass('panel');
            $alert_modal.animate({marginLeft : '280px'}, 300);
        });
    }

    var init = function () {
        var ie = isIE();
        if (ie > 0 && ie < 8) {
            alert('El sistema de notificaciones no es soportado por Internet Explorer ' + ie + ', Se recomienda usar Internet Explorer 8 o superior, Firefox o Chrome.');
            new Error('Faltan datos');
        }
        if (APP.length == 0 || USER.length == 0) {
            alert('Falta el nombre del sistema o el nombre de usuario para poder cargar el Módulo de Notificaciones.');
            throw new Error('Faltan datos');
        }
        
        if (TOKEN.length == 0 || TOKEN == 'ERROR' || TOKEN.split('.').length < 3) {
            alert('¡La sesión es inválida!');
            throw new Error('Sesión inválida');
        }

        $container = $('#container');
        $parent = $(window.parent.document);
    }

    init();
    
    app.$container = $container;
    app.$parent = $parent;
    
    app.nano = nano;
    app.ie = isIE();
    
    app.toType = function (obj) {
        return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
    }
       
    app.notifica = window.parent.window.Notifica;
    
    app.alerta = function (message, callback, title) {
        $alert_modal.html(nano(tmpl_alert, {
            title : title || 'Alerta', 
            message : message, 
            button_cancel : 'hide'
        }));
        showAlert();
        if (callback) {
            $alert_modal.find('.not-alert-footer .btn-primary').on('click', callback);
        }
    }

    app.confirm = function (message, callback, title) {
        $alert_modal.html(nano(tmpl_alert, {
            title : title || 'Confirmar', 
            message : message, 
            button_cancel : ''
        }));
        showAlert();
        if (callback) {
            $alert_modal.find('.not-alert-footer .btn-primary').on('click', callback);
        }
    }
    
    app.get = function (url, data, success, type) {
        if (App.toType(data) === 'function') {
            success = data;
            data = {};
        }
        
        data.app = APP;
        data.user = USER;
        
        if (SERVER === SERVER1) {
            $.ajax({
                url : SERVER + url,
                data : type === 'POST' ? JSON.stringify(data) : data,
                beforeSend    : function (request) {
                    request.setRequestHeader('authorization', TOKEN);
                },
                type : type || 'GET',
                contentType : 'application/json; charset=utf-8',
                dataType : 'json',
                success : function (response) {
                    success.apply(window, [response]);
                },
                error : function (xhr) {
                    if (xhr.status === 403) {
                        App[App.notifica.isOpen() ? 'loading' : 'growl'].error('No tiene los permisos necesarios')
                    }
                }
            });
        } else {
            data.token = TOKEN;
            $.ajax({
                url: SERVER + url,
                data: data,                
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                dataType: 'jsonp',
                jsonpCallback : 'callback',
                success: function (response) {
                    success.apply(window, [response]);
                }
            });
        }
    }
    
    app.position = function (el) {
        if (!el)
            return {top: 0, left: 0};

        var y = 0;
        var x = 0;
        while (el.offsetParent) {
            x += el.offsetLeft;
            y += el.offsetTop;
            el = el.offsetParent;
        }
        return {top: y, left: x};
    }
    
    app.empty = function (value) {
        return value === null || value.length === 0 || /^\s+$/.test(value);
    }
    
    app.truncate = function (text, length) {
        return text.length > length ? (text.substring(0, Math.min(length, text.length)) + '...') : text;
    }
       
    app.preg_quote = function (str) {
        return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
    }
    
    app.highlight = function (data, search) {
        return data.replace(new RegExp("(" + App.preg_quote(search) + ")", 'gi'), "<query>$1</query>");
    }

    return app;

}(App || {}, jQuery));

// Loading App
App.loading = (function () {

    var $loading = $('#container .not-loading');

    var loading = {};

    loading.show = function (text) {
        $loading.html(text || 'Cargando...').fadeIn(100);
    }

    loading.hide = function () {
        $loading.fadeOut(200);
    }
    
    loading.success = function (text) {
        $loading.addClass('not-loading-success').html(text || '¡Correcto!').fadeIn(100).delay(3000).fadeOut(function () {
            $(this).removeClass('not-loading-success');
        });
    }
    
    loading.error = function (text) {
        $loading.addClass('not-loading-error').html(text || '¡Error!').fadeIn(100).delay(3000).fadeOut(function () {
            $(this).removeClass('not-loading-error');
        });
    }

    return loading;

}());

// Alert App
App.alert = (function () {

    var tmpl_alert = $('#not-alert-tmpl').html();
    var $alerts = App.$parent.find('#not-alerts');

    var alert = {};
    
    alert.show = function (data) {
        var $alert = $($.parseHTML(App.nano(tmpl_alert, data)));
        $alert.find('.not-close').on('click', function () {
            $alert.children('div').off('mouseover');
            $alert.fadeOut(function () {
                setTimeout(function () {
                    $alert.remove();
                }, 500);
            });
        });
        $alerts.prepend($alert);
        $alert.animate({opacity : 1}).delay(5000).fadeOut(function () {
            $alert.children('div').off('mouseover');
            setTimeout(function () {
                $alert.remove();
            }, 500);
        });
        $alert.children('div').on('mouseover', function () {
            $alert.stop().css({opacity : 1});
        }).on('click', function () {
            App.message.open(data.id);
        });
    }

    return alert;

}());

// Growl App
App.growl = (function () {

    var $growls = App.$parent.find('#anb-messages'); //not-growls
    var tmpl_growl = $('#not-growl-tmpl').html();
    var delay = 5000;
    
    var hide = function ($growl) {
        $growl.fadeOut(1500, function () {
            setTimeout(function () {
                $growl.remove();
            }, 1000);
        });
    }
    
    var growl = {};
    
    growl.show = function (data) {
        
        var $growl = $(App.nano(tmpl_growl, data));
        $growl.find('.not-close').on('click', function () {
            $growl.off('mouseover');
            hide($growl);
        });       
        $growls.append($growl);
        $growl.animate({opacity : .95}, function () {
            var stop = false;
            $growl.on('mouseover', function () {
                stop = true;
                $growl.stop().css({opacity : .95});
            });
            $growl.on('mouseout', function () {
                stop = false;
                setTimeout(function () {
                    if (!stop) {
                        hide($growl);
                    }                
                }, data.delay || delay);
            });
            setTimeout(function () {
                if (!stop) {
                    hide($growl);
                }                
            }, data.delay || delay);
        });
        
        if (data.callback) {
            data.callback.apply(window, [$growl[0]]);
        }
    }
       
    growl.error = function (msg, callback, title, delay) {
        App.growl.show({
            type : 'error', 
            title : title || '¡Error!', 
            message : msg || 'Ha ocurrido un error.', 
            delay : delay || 5000,
            callback : callback || null
        });
    }
    
    growl.success = function (msg, callback, title, delay) {
        App.growl.show({
            type : 'success', 
            title : title || '¡Correcto!', 
            message : msg || 'La operación se realizó correctamente.', 
            delay : delay || 5000,
            callback : callback || null
        });
    }
    
    growl.notification = function (msg, callback, title, delay) {
        App.growl.show({
            type : 'warning', 
            title : title || '¡Notificación!', 
            message : msg || 'Hay nuevas notificaciones.', 
            delay : delay || 30000,
            callback : callback || null
        });
    }

    return growl;

}());

// Messages
App.message = (function () {
    
    var tmpl_item = $('#not-item-tmpl').html();
    var tmpl_details = $('#not-details-tmpl').html();
    
    var $list = null;    
    var $total = null;
    
    var ID = -1;
    
    var LOAD = true;
    
    var list = function (query) {
        if (!LOAD) {
            return false;
        }
        var params = {};
        params.id = ID;
        if (query) {
            params.query = query;
        }
        App.loading.show(query ? 'Buscando notificaciones' : 'Obteniendo notificaciones');
        App.get(query ? 'search' : 'list', params, function (data) {
            App.loading.hide();
            if (data.length) {
                var items = '<div>';
                for (var i = 0, l = data.length; i < l; i++) {
                    data[i].DATE = data[i].DATE_CREATED;
                    data[i].DATE_CREATED = App.datetime.last(data[i].DATE_CREATED);
                    data[i].MESSAGE = App.truncate(data[i].MESSAGE, 100);
                    data[i].QUERY = '';
                    if (query) {
                        data[i].MESSAGE = App.highlight(data[i].MESSAGE, query); 
                        data[i].TITLE   = App.highlight(data[i].TITLE, query);
                        data[i].SENDER  = App.highlight(data[i].SENDER, query);
                        data[i].QUERY   = query;
                    }
                    items += App.nano(tmpl_item, data[i]);
                }
                items += '</div>';
                if (data.length === 10) {
                    items += '<a href="#" class="not-more">Más  ' + (query ? 'resultados' : 'notificaciones') + '</a>';
                }
                var $items = $($.parseHTML(items));
                $items.find('.not-close').on('click', function () {
                    deleteItem(this);
                });
                $items.find('.not-item > div').on('click', function () {
                    read(this.parentNode);
                });
                $items.filter('.not-more').on('click', function (e) {
                    e.preventDefault();
                    list(query);
                    $(this).remove();
                });
                if (ID === -1) {
                    $list.html('');
                }
                $list.append($items);
                ID = data[l-1].ID;
                LOAD = true;
            } else {
                if (query) {
                    if ($list.find('[data-query=' + query + ']').length === 0) {
                        $list.html('').append('<div class="not-no-results">No hay resultados para la búsqueda.</div>');                    
                    }
                } else {
                    $list.find('.not-more, .not-no-results').remove();
                }
                LOAD = false;
            }
        });
    }
    
    var total = function (callback) {
        App.get('total', function (data) {
            var msg = '';
            if (data.total_send && parseInt(data.total_send) > 0) {
                var total = data.total_send;
                $total.html(total).css({display : 'block'});
                msg = 'Tiene <strong>' + (total > 1 ? total + '</strong> nuevas' : 'una nueva</strong>') + ' notificaci' + (total > 1 ? 'ones' : 'ón') + '.';
            } else {
                $total.html('').hide();
            }
            if (data.total_no_read && parseInt(data.total_no_read) > 0) {
                var total = data.total_no_read;
                msg += msg.length ? '<br>' : '';
                msg += 'Tiene <strong>' + (total > 1 ? total : 'una') + '</strong> notificaci' + (total > 1 ? 'ones' : 'ón') + ' <ins>pendiente(s)</ins> para leer.';
            }
            if (msg.length) {
                msg += '<br><a href="#">Ir a la bandeja de notificaciones</a>';                
                App.growl.notification(msg, function (o) {
                    $(o).find('a').on('click', function (e) {
                        e.preventDefault();
                        App.$parent.find('.not-open').click();
                    });
                });
            }           
            
            if (callback) {
                callback.apply(window);
            }
        });
    }
        
    var read = function (el) {
        var $el = $(el),
            position = App.position(el),
            id = $el.data('id');
    
        var show = function (id, top) {
            var $el = App.$parent.find('#not-details-' + id),
                elH = $el.height(),
                winH = $(window).height(),
                topCaret = 10,
                css = {display: 'block'},
                width = $(window.parent.window).width();
            if (width > 768) {
                top -= App.$container.find('.not-body').scrollTop();
                if (top + elH > winH) {
                    topCaret = elH + 10 - (winH - top);
                    if (topCaret > elH - 18) {
                        topCaret = elH - 18;
                    }
                    top = winH - elH - 2;
                }
                css.top = top;
                css.left = width - 670;
            } else {
                css.top = 38;
                css.left = width - 275;
            }
            $el.css(css).find('.not-caret').css({top : topCaret});
        }

        App.$parent.find('.not-details').hide();

        if (App.$parent.find('#not-details-' + id).length) {
            show(id, position.top);
        } else {
            App.loading.show('Obteniendo notificación');
            App.get('read', {id : id}, function (data) {
                if (data.ID) {
                    App.loading.hide();
                    $el.addClass('read');
                    var query = $el.data('query');
                    if (query.length) {
                        data.MESSAGE = App.highlight(data.MESSAGE, query);
                        data.TITLE   = App.highlight(data.TITLE, query);
                        data.SENDER  = App.highlight(data.SENDER, query);
                    }
                    data.TYPE = data.TYPE.toLowerCase();
                    data.DATE_CREATED = App.datetime.datetimeLiteral(data.DATE_CREATED);
                    App.$parent.find('body').prepend($($.parseHTML(App.nano(tmpl_details, data))));
                    show(id, position.top);
                } else {
                    App.loading.error('Error al obtener la notificación.');
                }
            });
        }
    }
    
    var readAll = function () {
        App.loading.show();
        App.get('readAll', function (data) {
            if (data.msg === 'OK') {
                App.$container.find('.not-item').addClass('read');
                App.loading.success('¡Notificaciones leidas!');
            } else {
                App.loading.error('Error.- ' + data.msg);
            }
        });
    }
    
    var viewAll = function (callback_success) {
        App.get('viewAll', function (data) {
            if (data.msg !== 'OK') {
                App.loading.error('Error.- ' + data.msg);
            }
            if (callback_success) {
                callback_success.apply(window);
            }
        });
    }
        
    var deleteItem = function (el) {
        var $el = $(el).parent();
        App.confirm('¿Deséa eliminar la notificación?', function () {
            App.loading.show('Eliminando notificación');
            var id = $el.data('id');
            App.get('delete', {id : id}, function (data) {
                if (data.msg === 'OK') {
                    App.loading.success('¡Notificación eliminada!');
                    App.$parent.find('#not-details-' + id).remove();
                    $el.animate({opacity : 0}, function () {
                        $el.animate({height: 0, padding: 0, margin: 0, borderWidth: 0}, function () {
                            setTimeout(function () {
                                $el.remove();
                            }, 500);
                        });
                    });
                } else {
                    App.loading.error('Error.- ' + data.msg);
                }
            });
        });
    }
    
    var sendForm = function ($form) {
        $form.on('submit', function (e) {
            e.preventDefault();
            var form = this;
            $form.find('.btn-primary')[0].disabled = true;
                
            $form.find('.required').on('keyup', function () {
                $(this).next().fadeOut(200);
            }).on('change', function () {
                setTimeout(function () {
                    $(this).next().fadeOut(200);
                }, 1);
            });
            var elements = form.querySelectorAll('.required');
            for (var i = elements.length - 1; i >= 0; i--) {
                if (App.empty(elements[i].value)) {
                    $(elements[i]).next().show();
                }
            }
            if ($form.find('.not-error:visible').length === 0) {
                var data = {
                    receiver: form.receiver.value,
                    type: form.type.value,
                    title: form.title.value,
                    message: form.message.value
                };
                App.message.create(data);
            } else {
                $form.find('.btn-primary')[0].disabled = false;
            }
        });
    }
    
    var init = function () {
        
        $list = $('#not-items');;
        $total = App.$parent.find('#not-number-state');
          
        var $form = $('#form-add');
        var $search = $('#container-search');
        var $inputSearch = $search.children('input');
        
        total(function () {
            list();
            if (App.notifica.isOpen()) {
                viewAll();
            }
        });
        
        sendForm($form);
                
        $('#btn-read-all').on('click', function () {
            readAll();
        });
        
        $('#btn-add').on('click', function () {
            App.$container.addClass('panel').find('.not-add').animate({marginLeft : '2px'}, 300);
            setTimeout(function () {$('#title').focus();}, 500);
            $form[0].reset();
            $form.find('.not-error').hide();
            App.$parent.find('.not-details').hide();
        });
        
        $('#btn-cancel').on('click', function () {
            App.$container.removeClass('panel').find('.not-add').animate({marginLeft : '280px'}, 300);
            $form.find('.btn-primary')[0].disabled = false;
        });
        
        $('#btn-search').on('click', function () {
            App.$parent.find('.not-details').remove();
            $search.fadeIn(500, function () {
                $search.children('input').focus();
            });
        });
        $search.children('span').on('click', function () {
            App.$parent.find('.not-details').remove();
            $search.fadeOut(500);
            $inputSearch.val('');
            ID = -1;
            LOAD = true;
            list();
        });
        $inputSearch.on('keypress', function (e) {
            if (e.keyCode === 13) {
                ID = -1;
                LOAD = true;
                list(this.value);
                LOAD = false;
            }
        });
        
        var $scroll = App.$container.find('.not-body');
        var $height = $scroll.children('div');
        App.$container.find('.not-body').on('scroll', function(e) {
            e.preventDefault();
            App.$parent.find('.not-details').hide();
            if ($height.height() - $scroll.scrollTop() < $(window).height() - 20) {
                $list.find('.not-more').remove();
                var query = $inputSearch.val();
                list(query.length ? query : false);
                LOAD = false;
            }
	});
        
        // Update date for not-items
        setInterval(function () {
            var items = document.querySelectorAll('#not-items .not-date');
            for (var i = items.length - 1; i >= 0; i--) {
                items[i].innerHTML = App.datetime.last(items[i].getAttribute('data-date'));
            }
        }, 10000);
    }
    
    init();
    
    var newItem = function (data) {
        data.DATE = data.DATE_CREATED;
        data.DATE_CREATED = App.datetime.last(data.DATE_CREATED);
        data.QUERY = '';
        var $item = $($.parseHTML(App.nano(tmpl_item, data)));
        $item.children('.not-close').on('click', function () {
            deleteItem(this);
        });
        $item.children('div').on('click', function () {
            read(this.parentNode);
        });
        $list.prepend($item);
    }

    var message = {};
    
    message.viewAll = viewAll;
    message.read = read;

    message.create = function (data) {        
        data.sender = USER;
        data.receiver = data.receiver.toUpperCase();
        data.user = USER;
        data.app = APP;
        data.token = TOKEN;

        App.loading.show('Enviando notificación');
        App.io.send(data);
        /*$('#btn-cancel').click();
        
        App.get('create', data, function (response) {
            if (response === 'OK') {
                App.loading.success('¡Notificación enviada!');
                data.id = response.id;
                data.date = response.date;
                data.date_created = App.datetime.last(response.date);
                data.type = data.type.toLowerCase();
                data.message = App.truncate(data.message);
                //App.io.send(data);
            } else {
                App.loading.error(response);
            }
            $('#btn-cancel').click();
        }, 'POST');*/
    }
    
    message.add = function (data) {
        if (App.notifica.isOpen()) {
            viewAll(function () {
                newItem(data);
            });
        } else {
            var count = $total.html();
            $total.html(App.empty(count) ? 1 : parseInt(count)+1).show();
            newItem(data);
        }
    };
    
    message.open = function (id) {
        var el = App.$container.find('.not-item[data-id=' + id + ']')[0];
        if (el) {
            if (App.notifica.isOpen()) {
                setTimeout(function () {
                    App.message.read(el);
                }, 1);
            } else {
                App.$parent.find('.not-open').click();
                setTimeout(function () {
                    App.message.read(el);
                }, 1000);
            }
        }
    }

    return message;

}());

// Datetime
App.datetime = (function () {

    var formatDate = 'dd/MM/YYYY',
        months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        days = ["dom", "lun", "mar", "mié" , "jue", "vie" , "sáb"],
        dayslong = ["Domingo", "Lunes", "Martes", "Miércoles" , "Jueves", "Viernes" , "Sábado"];
        
    
    var datetime = {};

    datetime.isDate = function (date) {
        if (App.toType(date) == 'string') {
            date = App.datetime.convert(date);
        }
        return App.datetime.isValid(date);
    }

    datetime.isValid = function (date) {
        if (App.toType(date) != 'date')
            return false;
        return !isNaN(date.getTime());
    }

    datetime.convert = function (date) {
        date = date.split('/');
        return formatDate == 'dd/MM/YYYY' ? new Date(date[2], date[1]-1, date[0]) : 
               formatDate == 'MM/dd/YYYY' ? new Date(date[2], date[0]-1, date[1]) : 
               new Date(date[0], date[1]-1, date[2]);
    }

    datetime.now = function (format) {
        return App.datetime.replace(new Date(), format || formatDate);
    }

    datetime.dateLiteral = function (date) {
        return datetime.format(date, 'dd de MMM del YYYY');
    }
    
    datetime.timeLiteral = function (date) {
        return datetime.format(date, 'HH:mm');
    }

    datetime.datetimeLiteral = function (date) {
        return datetime.format(date, 'dddd dd de MMM del YYYY a la(s) HH:mm');
    }
    
    datetime.format = function (date, format) {
        var d = new Date(date);
        if (App.datetime.isDate(d)) {
            return App.datetime.replace(d, format || formatDate);
        }
        return date;
    }

    datetime.replace = function (date, format) {
        var monthLiteral = format.indexOf('MMM') != -1;
        return App.array.replace(format, [
                "dddd", "ddd", "dd", monthLiteral ? 'MMM' : 'MM', 'YYYY', 'HH', 'mm', 'ss'
            ], [
                dayslong[date.getDay()],
                days[date.getDay()],
                (date.getDate() < 10 ? '0' : '') + date.getDate(), 
                monthLiteral ? months[date.getMonth()] : ((date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1)), 
                date.getFullYear(), 
                (date.getHours() < 10 ? '0' : '') + date.getHours(), 
                (date.getMinutes() < 10 ? '0' : '') + date.getMinutes(), 
                (date.getSeconds() < 10 ? '0' : '') + date.getSeconds()
            ]);
        }
    
    datetime.last = function (date) {
        var now = new Date();
        var days = App.datetime.daysBetweenTwoDates(date, now);
        if (days > 31) {
            return App.datetime.dateLiteral(date);
        } else {
            if (days > 7) {
                var weeks = parseInt(days / 7);
                return 'Hace ' + weeks + ' semana' + (weeks > 1 ? 's' : '');
            } else {
                if (days >= 1) {
                    return App.datetime.format(date, 'ddd a la(s) HH:mm');
                } else {
                    var hours = App.datetime.hoursBetweenTwoDates(date, now);
                    if (App.datetime.isYesterday(date, hours)) {
                        return 'Ayer a la(s) ' + App.datetime.timeLiteral(date);
                    } else {
                        if (hours >= 1) {
                            return 'Hace ' + hours + ' hora' + (hours > 1 ? 's' : '');
                        } else {
                            var minutes = App.datetime.minutesBetweenTwoDates(date, now);
                            if (minutes >= 1) {
                                return 'Hace ' + minutes + ' minuto' + (minutes > 1 ? 's' : ''); 
                            } else {
                                var seconds = App.datetime.secondsBetweenTwoDates(date, now);
                                return 'Hace ' + seconds + ' segundo' + (seconds > 1 ? 's' : '');
                            }
                        }
                    }
                }
            }        
        }
    }

    datetime.secondsBetweenTwoDates = function (date1, date2, absolute) {
        return App.datetime.betweenTwoDates(date1, date2, "s", absolute);
    }

    datetime.minutesBetweenTwoDates = function (date1, date2, absolute) {
        return App.datetime.betweenTwoDates(date1, date2, "i", absolute);
    }

    datetime.hoursBetweenTwoDates = function (date1, date2, absolute) {
        return App.datetime.betweenTwoDates(date1, date2, "h", absolute);
    }

    datetime.daysBetweenTwoDates = function (date1, date2, absolute) {
        return App.datetime.betweenTwoDates(date1, date2, "d", absolute);
    }

    datetime.betweenTwoDates = function (date1, date2, format, absolute) {
        var formats = {s : 1000, i : 60*1000, h : 60*60*1000, d : 24*60*60*1000};
        var newDate = parseInt((date2.getTime() - new Date(date1).getTime())/formats[format]);
        
        if (typeof absolute !== 'undefined' && absolute !== false) {
            return Math.abs(newDate);
        }
        return newDate;
    }
    
    datetime.isYesterday = function (date, hours) {
        date = date.split(' ');
        var time = date[1].split(':');

        return hours >= parseInt(time[0]);
    }

    return datetime;

}());

// Array
App.array = (function () {

    var array = {};
    
    array.replace = function (text, find, replace) {
        for (var i = 0, l = find.length, regex; i < l; i++) {
            regex = new RegExp(find[i], "g");
            text = text.replace(regex, replace[i]);
        }
        return text;
    };

    return array;

}());

App.notification = (function () {

    var DEFAULT = 0;
    var DENIED = 1;
    var GRANTED = 2;

    var icon = 'img/anb-icon.png';
    var timeout = 10000;

    var notification = {};

    notification.DEFAULT = DEFAULT;
    notification.DENIED = DENIED;
    notification.GRANTED = GRANTED;

    notification.get = function () {
        return window.Notification;
    }

    notification.getSupported = function () {
        return !!App.notification.get();
    }

    notification.getPermissionStatus = function () {
        return App.notification.translatePermissionStatus(App.notification.get().permission);
    }

    notification.translatePermissionStatus = function (status) {
        var options = {
            'granted': GRANTED,
            'denied' : DENIED,
            'default': DEFAULT
        };
        return options[status] || options['default'];
    }

    notification.askForPermissions = function () {
        App.confirm('¿Desea activar las notificaciones de escritorio?', function () {
            if (App.notification.getSupported()) {
                var notif = App.notification.get();
                notif.requestPermission();
                $('#btn-notiweb').parent().html('<span>Notificaciones de escritorio habilitada.</span>');
                App.loading.success('¡Notificaciones de escritorio habilitadas!');
            }
        });
    }

    notification.create = function (id, title, msg, tag) {
        var options = {body: msg};

        if (tag) {
            options.tag = tag;
        }
        if (icon) {
            options.icon = icon;
        }

        var notif = new Notification(title, options);
                
        notif.addEventListener("click", function () {
            window.parent.window.focus();
            App.message.open(id);
        });

        if (timeout && typeof (timeout) === 'number') {
            setTimeout(App.notification.close, timeout, notif);
        }

        return notif;
    }

    notification.show = function (id, title, msg, tag) {
        if (App.notification.getSupported()) {
            if (App.notification.getPermissionStatus() === GRANTED) {
                App.notification.create(id, title, msg, tag);
            } else {
                var notif = App.notification.get();
                notif.requestPermission(function (status) {
                    if (App.notification.translatePermissionStatus(status) === GRANTED) {
                        App.notification.create(id, title, msg, tag);
                    }
                });
            }
        }
        return null;
    }

    notification.close = function (notif) {
        notif.close();
    }

    return notification;

}());

// Módulo IO.JS
App.io = (function (io) {

    var socket = null;

    var startup = function (data) {
        App.loading.success(data.message);
    }

    var newNotification = function (data) {
        if (USER === data.receiver) {
            App.message.add(data);
            if (App.notification.getSupported() && App.notification.getPermissionStatus() === App.notification.GRANTED) {
                App.notification.show(data.ID, data.TITLE, data.MESSAGE);
            } else {
                App.alert.show(data);
            }
        }
    };

    var init = function () {
        
        socket = io(SERVER);

        socket.on('startup', startup);
        socket.on('new notification', newNotification);
        socket.on('error', function (data) {
            if (App.notifica.isOpen()) {
                App.loading.error(data.msg);
            } else {
                App.growl.error(data.msg);
            }
        });
        socket.on('success', function (data) {
            if (App.notifica.isOpen()) {
                App.loading.success(data.msg);
            } else {
                App.growl.success(data.msg);
            }
        });

        socket.emit('join app', {app : APP, user : USER});
    }

    init();

    io.send = function (data) {
        socket.emit('send notification', data);
    }

    return io;

}(io));

// Módule on load File
App.load = (function () {

    var init = function () {
        
        var $btn_notiweb = $('#btn-notiweb');
        if (App.notification.getSupported()) {
            if (App.notification.getPermissionStatus() === App.notification.GRANTED) {
                $btn_notiweb.parent().html('<span>Notificaciones de escritorio habilitada.</span>');
            } else {
                $btn_notiweb.on('click', function () {
                    App.notification.askForPermissions();
                });    
            }
        } else {
            $btn_notiweb.parent().html('<span class="not-support">Notificaciones de escritorio no soportada.</span>');
        }
    }

    init();

}());
