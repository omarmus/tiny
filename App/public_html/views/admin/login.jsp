<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<div class="form-login">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Debe iniciar su sesión</h3>
        </div>
        <html:form action="/login.do" styleClass="panel-body">
            <html:errors property="message"/>
            <c:if test="${!empty message}">
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <c:out value="${message}" escapeXml="false" />
            </div>
            </c:if>
            <div class="input-group">
                <input type="text" name="usuario" class="form-control" value="<c:out value='${AuthForm.usuario}' />" placeholder="Usuario" />
                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
            </div>
            <html:errors property="usuario"/>
            <div class="input-group bottom">
                <input type="password" name="clave" class="form-control" value="<c:out value='${AuthForm.clave}' />" placeholder="Contraseña" />
                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
            </div>
            <html:errors property="clave"/>
            <div class="form-group" style="width: 100%; padding: 0;">
                <html:select property="aduana" styleClass="form-control">
                    <option value="-">Seleccionar una aduana...</option>
                    <option value="211">211 Aeropuerto El Alto</option>
                    <option value="311">311 Aeropuerto Cochabamba</option>
                    <option value="711">711 Aeropuerto Viru-Viru</option>
                </html:select>
                <html:errors property="aduana"/>
            </div>
            <button class="btn btn-primary btn-block" type="submit">Ingresar</button>
        </html:form>        
    </div>
    <div class="text-center">
        <img src="img/anb.png" />
    </div>
</div>



