<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-search"></i> <strong>Buscar Guías Courier</strong> <sub>con Autorización de Retiro</sup></h3>
    </div>
    <div class="panel-body">
        <form action="" class="form-horizontal" id="form-reporte">
            <div class="form-group">
                <label class="col-md-3 control-label">Tipo de reporte:</label>
                <div class="col-md-8">
                    <select class="form-control required" name="estado" data-label="Seleccione el tipo de reporte que desea consultar">
                        <option value="">Seleccione...</option>
                        <option value="PENDIENTE">GUÍAS COURIER PENDIENTES DE RETIRO</option>
                        <option value="AUTORIZADO">GUÍAS COURIER CON AUTORIZACIÓN DE RETIRO</option>
                        <option value="IMPRIMIDO">GUÍAS COURIER CON CONSTANCIA DE ENTREGA</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Nro. Manifiesto:</label>
                <div class="col-md-3">
                    <input class="form-control integer" name="nro_manif" />
                </div>
                <label class="col-md-2 control-label">Doc. Embarque:</label>
                <div class="col-md-3">
                    <input class="form-control integer" name="doc_embarque" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Fecha registro Manifiesto:</label>
                <div class="col-md-3">
                    <input class="form-control datepicker" name="fecha_manif" />
                </div>
                <label class="col-md-2 control-label">Técnico aduanero: </label>
                <div class="col-md-3">
                    <input class="form-control" name="usuario" />
                </div>
            </div>
            <div class="row">
                <label class="col-md-3 control-label">Tipo de mercancía:</label>
                <div class="col-md-2">
                    <select class="form-control" name="tipo">
                        <option value="TODOS">TODOS</option>
                        <option value="SACA VERDE">SACA VERDE</option>
                        <option value="SACA AZUL">SACA AZUL</option>
                    </select>
                </div>
                <div class="col-md-6 text-right btn-container">
                    <button type="reset" id="limpiar" class="btn btn-default"><i class="glyphicon glyphicon-remove-circle"></i> Limpiar</button>
                    <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Buscar Courier</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="container-reporte"></div>

<script>
    var DT = null;
    $(document).ready(function () {
        DT = new Anb.datatable();
        DT.btnView('.view', 'courierDetalles.do');

        var $form_reporte = $('#form-reporte'),
            $container_reporte = $('#container-reporte');
        $form_reporte.on('submit', function (e) {
            e.preventDefault();
            if (Anb.validate.run(this)) {
                Anb.loading.show("Buscando guías courier, espere por favor");
                $.post('autorizacionReporte.do', $form_reporte.serialize(), function (response) {
                    Anb.loading.hide();
                    $container_reporte.html(response);
                });
            }
        });

        $('#limpiar').on('click', function() {
            Anb.form.reset('#form-reporte');
            $container_reporte.html('');
        });
    });
</script>