<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><strong>Buscar Guías Courier Retenidas</strong></h3>
    </div>
    <div class="panel-body">
        <form action="" class="form-horizontal" id="form-reporte">
            <div class="form-group">
                <label class="col-md-3 control-label">Nro. Manifiesto:</label>
                <div class="col-md-3">
                    <input class="form-control integer" name="nro_manif" />
                </div>
                <label class="col-md-2 control-label">Doc. Embarque:</label>
                <div class="col-md-3">
                    <input class="form-control integer" name="doc_embarque" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Fecha registro Manifiesto:</label>
                <div class="col-md-3">
                    <input class="form-control datepicker" name="fecha_manif" />
                </div>
                <label class="col-md-2 control-label">Técnico aduanero: </label>
                <div class="col-md-3">
                    <input class="form-control" name="usuario" />
                </div>
            </div>
            <div class="row">
                <label class="col-md-3 control-label">Tipo de mercancía:</label>
                <div class="col-md-2">
                    <select class="form-control" name="tipo">
                        <option value="TODOS">TODOS</option>
                        <option value="SACA VERDE">SACA VERDE</option>
                        <option value="SACA AZUL">SACA AZUL</option>
                    </select>
                </div>
                <div class="col-md-6 text-right container-buttons">
                    <button type="reset" id="limpiar" class="btn btn-default"><i class="glyphicon glyphicon-remove-circle"></i> Limpiar</button>
                    <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Buscar Courier</button>
                </div>
            </div>
            <input type="hidden" name="estado" value="RETENIDO" />
        </form>
    </div>
</div>
<div id="container-reporte"></div>

<script>
    var DT = null;
    $(document).ready(function () {
        DT = new Anb.datatable();
        DT.btnView('.view', 'courierDetalles.do');

        var $form_reporte = $('#form-reporte'),
            $container_reporte = $('#container-reporte');
        $form_reporte.on('submit', function (e) {
            e.preventDefault();
            Anb.loading.show("Buscando guías courier retenidas");
            $.post('autorizacionReporte.do', $form_reporte.serialize(), function (response) {
                Anb.loading.hide();
                $container_reporte.html(response);
            });
        });

        $('#limpiar').on('click', function() {
            $container_reporte.html('');
        });
    });
</script>