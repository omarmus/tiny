<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <i class="glyphicon glyphicon-send"></i> Registrar Retención para: ${CourierForm.anio_manif}/${CourierForm.aduana}-${CourierForm.nro_manif} - ${CourierForm.doc_embarque}
        <div class="pull-right">
            <span class="label" style="color: crimson;">ESTADO:</span>
            <span class="label label-${CourierForm.estado == 'POR REVISAR' ? 'info' : CourierForm.estado == 'RETENIDO' ? 'danger' : 'warning'}">
                ${CourierForm.estado}
            </span>
        </div>
    </h4>
</div>
<html:form action="courierGuardarRetencion.do" method="post" styleId="form-retension">
    <div class="modal-body">
        <%@ include file="datos_courier.jsp" %>
        <div class="form-group" style="width: 20%;">
            <label>Peso Real <strong>*</strong></label> 
            <html:text property="peso_real" maxlength="10" styleClass="form-control required decimal text-right" />
        </div>
        <div class="form-group" style="width: 20%;">
            <label>Peso Observado <strong>*</strong></label> 
            <html:text property="peso_observado" maxlength="10" styleClass="form-control required decimal text-right" value="${CourierForm.peso - CourierForm.peso_real}"/>
        </div>
        <div class="form-group" style="width: 60%;">
            <label>Observaciones <strong>*</strong></label> 
            <textarea type="text" class="form-control required" name="observacion" data-maxlength="500"></textarea>
        </div>
    </div>
    <div class="modal-footer"> 
        <button type="button" class="btn btn-default" data-dismiss="modal"> 
            <span class="glyphicon glyphicon-ban-circle"></span> Cancelar 
        </button>
        <c:if test="${CourierForm.estado == 'POR REVISAR'}">
        <button type="button" class="btn btn-default" id="atras">
            <span class="glyphicon glyphicon-chevron-left"></span> Atrás 
        </button>
        </c:if>
        <button type="submit" class="btn btn-primary save-loading" disabled="disabled"> 
            <span> 
                <span class="glyphicon glyphicon-ok"></span> <span class="title-submit">Registrar retención</span> 
            </span> 
            <span> 
                <img src="img/loader.gif"> <span>Procesando</span> 
            </span> 
        </button>
    </div>
</html:form>
<script>
    $('#form-retension').on('submit', function(event) {
        event.preventDefault();
        Anb.form.submit(this, 'courierGuardarRetencion.do', {
            validate : function (form) {
                if (parseFloat(form.peso_observado.value) == 0) {
                    Anb.alert('<div class="alert alert-danger">El peso observado debe ser mayor a cero para poder retener la guía.</div>', function () {
                        form.peso_observado.focus();
                    });
                    return false;
                }
                return true;
            },
            success : function (data) {
                Anb.modal.update('courierDetalles.do?' + getPkGuia($('#pk')));
            }
        })
        return false;
    });
</script>