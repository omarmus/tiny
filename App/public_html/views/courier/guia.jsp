<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<form id="form-impresion" method="post">
    <div class="panel panel-default imprimir">
        <div class="panel-heading">
            <h3 class="panel-title"><strong>Guía Courier</strong></h3>
        </div>
        <div class="panel-body" style="font-size: 0;">
            <%@ include file="datos_courier.jsp" %>
        </div>
        <div class="panel-footer text-right">
            <c:if test="${CourierForm.estado == 'IMPRIMIDO'}">
                <strong class="pull-left label label-danger">GUÍA COURIER CON CONSTANCIA DE ENTREGA</strong>
            </c:if>
            <button type="button" id="btn-detalles" class="btn btn-default"><i class="glyphicon glyphicon-send"></i> Ver más detalles</button> 
            <button type="button" id="btn-imprimir${CourierForm.estado == 'IMPRIMIDO' ? '' : '-save'}" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Imprimir guía courier</button>
        </div>
    </div>
</form>
<script>
    var $form_impresion = $('#form-impresion'),
        administracion = $('#administracion').html();
    $('#btn-imprimir-save').on('click', function () {
        Anb.confirm('Se procederá a <strong>registrar</strong> la Constancia de Entrega del Courier e <strong>imprimirla</strong>. ¿Deséa continuar?', function () {
            Anb.loading.show('Guardando constancia de entrega');
            $.get('impresionImprimirGuia.do', $form_impresion.serialize(), function (response) {
                Anb.loading.hide();
                if (response.state === 'SUCCESS') {
                    Anb.message.ok(response.data);
                    createPDF($form_impresion[0]);
                } else {
                    Anb.message.error(response.data);
                }
            });
        });
    });
    $('#btn-imprimir').on('click', function () {
        createPDF($form_impresion[0]);
    });
    $('#btn-detalles').on('click', function () {
        Anb.modal.show('courierDetalles.do?' + getPkGuia($('#pk')), 'lg');
    });
    
    var getPkGuia = function ($el) {
        var pk = 'fecha-partida=' + $el.data('fecha-partida');
            pk += '&aduana=' + $el.data('aduana');
            pk += '&nro-viaje=' + $el.data('nro-viaje');
            pk += '&doc-embarque=' + $el.data('doc-embarque');
            pk += '&view=YES';
        return pk;
    }
    
    var createPDF = function (form) {
        
        var copia = ['COURIER', 'ANB', 'CONCESIONARIO'];
        var imgData = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QCKRXhpZgAATU0AKgAAAAgABwEaAAUAAAABAAAAYgEbAAUAAAABAAAAagEoAAMAAAABAAIAAAExAAIAAAAQAAAAclEQAAEAAAABAQAAAFERAAQAAAABAAAAAFESAAQAAAABAAAAAAAAAAAAAABgAAAAAQAAAGAAAAABcGFpbnQubmV0IDQuMC41AP/bAEMAAgEBAgEBAgICAgICAgIDBQMDAwMDBgQEAwUHBgcHBwYHBwgJCwkICAoIBwcKDQoKCwwMDAwHCQ4PDQwOCwwMDP/bAEMBAgICAwMDBgMDBgwIBwgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIADoAZAMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/APxeuP8Aj4k/3jTKfcf8fEn+8aZX64fn0dgooooGFFFdB4C+GGsfEmW4XSbeOZbPb5zPKqBN2dvU5Odp6A9K58XjKGFpOviZqEFu27JX0Wr89PU9TJcjzDN8bDLsqoTrVp35YQi5Sdk27RSbdkm32SbeiOfor1rS/wBkHXrh/wDStQ0u1TOPkZ5G/LaB+tdHpP7Hen2uH1DWrqZVGWEMKwgevJLcYr4/FeI3D1DR4hSfaKk/xSt+J+4ZN9FfxOzGzjljpR71J04W9U5c/wB0WeEXNhPZwwySwyRx3KeZEzKQJFyVyPUZBH4VDX0J8a/D/hnwf8DodOj23LWspi01zIHlWVnLSfMO2C2R06DrivnuvU4X4hWcYWWKjTcUpyir9Uno18mr9nddD5Dxe8MZ8C5vRyitiYVpzo06kuR35JTXvQb2dpJuL+1Bxk0r2CiiivpD8qL+l/8AHu3+9/hRRpf/AB7t/vf4UUHFU+JlO4/4+JP940yn3H/HxJ/vGmUHZHYK0PCfhLVPHviax0XQ9NvtY1jVJlt7Oys4WmnuZG6IiKCWJ9qz69c/Ym/bF179hz44W3jfw/peiavdLA1ncW2o24fzIHILrHJ96FyBjenYkEMpKnyc9xGYUMurVsqpRq4iMW4QlLkjKVtE5Wdk31/FbrrwcKE68I4mTjBtXaV2l1dup9PfHv8A4IQfED4TfsweB9Y0XT9Y8XfE7WNSMWvaNppjmtNKgeJmjCvgZaNlCySFyhaTC/Ku9sj9nz/gkX+1L4auLqe18A6RpceoRopbWtZtUVDkEEpFK0nAY5GB+YxX2Tq//Bwb4D8T/sv+IPEnhmzh0n4k6LFDLH4W8RM4jvw0yJKLe4i+WUqjMwHyMdv3cZNeI3X/AAV9/aW+Kvh61vtJm+GHhO11CBZYmtNNnu541YA5/fO6bhjpgj5iD0GP4iyni/xtxuWV8Dm2DwtNe1nGUsUpxd21UUYRhL3qcU0oSUZppW5pNM/XqnEXCXB+Y4fPMFjqlGrFXhKlZu7jKDveLtKSvdab7K503g3/AIIlfHjxi0b+KPiR4F8H27ruaLR7GbUplPXafMEaj0yrn8a7DW/+CTP7Ov7Muj/2l8c/jHret7Srtb6triaXbT+yW0R85s56I54zx1r5b+KH7Ufxg8cWk3/CdfHrxRb2dxl5rXS5odEhYEcoPJCkrx074Pc14Jq3xK+HPgXUJLqxsZPE2rSMWkup2a4YtzgmWbPtyoPHr39DK+B+Pc0fLic3VKP8uBw8Yf8AlxUjTqR9WpHkcRfSSecXjQjise3/AM/qtR016wcvZpeiV+tyH9qHwPpHjDxv4w1/4bhB8M9I1m5g0C18uSDy7AbWEyCbDsrEn7/znjPTA8Qr0P4qftC6h8TPDkOmmGOxgaQyXKxE/vcMSi5zyo4J6Zbtxz55X9f8J4PGYTL44bGbw0V3zSskleckkpTk7ylKyu3d6s/JqGYZljpTxWaJKcnpFO/LBJRjG/aKSUUtFFJdNCiiivpDoL+l/wDHu3+9/hRRpf8Ax7t/vf4UUHFU+JlO4/4+JP8AeNex/sx/sOeLv2rfhP8AFrxh4duNJt9P+EOhf27qMN3KyTaioWWRoLYAEPKsFvcTEEj5ITXjlx/x8Sf7xr7d/wCCdv7efwx/ZG8GfDXw9r/h+18Qp4h8X6pe+O9QnN/HJ4c0y7sRo4NskMix3Un2K41CQrIkgXzgFAYmufFTqRhekrv+v+GPRoxi372x4R8FP2B/iP8AHD4NeNvH1j4f1rT/AAv4P8PnX4NQutIuvsniDF7b2f2WzmCeXJOXuNwUE5ETjqK8u0D4d+IfFl1FBpPh/XNUnnuGs4orPT5rh5J1Xe0SqikmRV+YqOQOSMV+nvwy/wCCmnwZ+FX7VfiL4xL491q+8M6l4S8M+FNM+H0Gk3ovdFbStR0lnMO5RZi0EOmzTxMJRIWu9rIr7yfOPBfxk/Z/8JfA7xf4E1j4tDxBqWpeMPEHxA8N32l2OtaXooup7GO0s7TU5FhjvY1cPcM62wZQY0VpSrE1yxxVbXmg+ltH21Xy/Hoa+yh/N/X9fcfC8fwT8aza3q2mL4N8XNqWgoJNUsxo1ybjTEIyGnj2bolI5y4AxzVuLS/iC/w0j1mOx8ZjwbZuIU1KO1uV0yFs7QvnhfLBzwBu6nHtX6G/tDf8FTPBep6J8AYPB3j2603WvBvjHw5P4zvfDttqVha3llYaZZW8kiPN+/uYI2W4jQTbpGCuduHG7F+KH7V/wD8D6N8bpPh140hvtF+JngHXNKjsNQtdY/tpdYv9SedbeC3KLp1rpyAq6sMyYXnDkgRKpKpy+0pX7XV7dO2nkRUweHnbnadu9nb0PnOx/wCCW3jrVvh38AfGCa14duND/aC1mPQLG4R5pJfDt3LMY4Uv12ZXzUV5U2FtyRv3XnwXWfhd4m8PeFbfXr7w34htPD97KIbXVJ9MnjsrpznCxzMoRidrYCk52nHQ198fsRf8FF/hh8NfFHw18J+PtSvk+Htn4I0GTULuDT5LiTw94p0TVr3ULKdIwNzq8Uz2zmMHKXfJwh297+yj/wAFAP2dv2LvEGpNY+IH8X/D/UrPwsLOwurPVb7xOl9b3NtcXNzcx3ONNt1spFnkhitgQ5jgUMcsaf1nEQbTg32sul+/l+l+pfsYNq0tLfifnNr37PHijwj4Q/tDWtH17RNSmvrWzs9Hv9Cv4LrUEuElZJomaERFcxbAhcSOXyisFcrht8KvFS+NF8Nnwv4lHiRumkHSp/t54z/qNnmdBn7vSv0M/Y7/AGyPhr+y58NfiN4f8V/G6Pxl4q8eeJpNQ0PxzYWGrXVx4dmm0LWLGLWZhcQxzLPDcXcW5U3yILhnjLFM1y+o/tzeG/hd/wAE6vEHwu/4WXN4u+LkOgzRad4t0k35aCK51/TLn+xYL6WOOYxxw2dxcOzbYt100aFjkHRYmrzcvJ1Svr167bL7xeyha/MfD2j/AAh8XeI7HVbnTfCXinULfQiy6lJa6RcTLpxUEsJiqERkAEkPjABPaug/aZ/Z31L9l/4lWvhnVNR0/VLm80HSPECT2W7yvK1Gwgvok+YA7kSdVY9CVJHBr9EdA/bx+Bmj/tCfCv4vWXxJa48WaVLocvj2PxDb647SpZaDptvLJp0Fsiw3V5LNDeJJNeMcv5RGUJc/FX/BSv48eGv2j/2nofE3hO8mvtIHg/wzpbSSQPCy3NpotnbXCbWAPyTRSJnoduQSCCdKOIqTqWlFpWv87rT8zOUYqN09b/geI6X/AMe7f73+FFGl/wDHu3+9/hRXYedU+JlO4/4+JP8AeNMp9x/x8Sf7xplB2R2CiiigYUUUUAFFFFABRRRQAUUUUAX9L/492/3v8KKNL/492/3v8KKDiqfEz//Z';        
        var pdf = new jsPDF('portrait', 'mm', 'letter');

        for(var i = 0; i < 3; i++) {
            if (i > 0) {
                pdf.addPage();
            }
            // Logo ANB
            pdf.addImage(imgData, 'JPEG', 21, 21, 23, 13); 

            pdf.roundedRect(20, 20, 25, 15, 1, 1);
            pdf.roundedRect(46, 20, 124, 15, 1, 1);
            pdf.setFontSize(10);
            pdf.setFontType("bold");
            pdf.text(83, 26, 'Constancia de entrega Courier');
            pdf.text(86, 31, 'RETIRO DE DOCUMENTOS');
            pdf.roundedRect(171, 20, 25, 15, 1, 1);

            pdf.setFontSize(8);
            pdf.setFontType("normal");
            pdf.text(20, 40, 'N° REGISTRO:');
            pdf.rect(40, 37, 28, 4);
            pdf.rect(100, 37, 25, 4);
            pdf.setFontSize(7);
            pdf.text(104, 40, form.tipo.value);

            pdf.setFontType("bold");
            pdf.text(171 - (i === 2 ? 5 : 0), 40, 'COPIA ' + copia[i]);

            pdf.text(20, 45, 'CODIGO ADUANA: ');
            pdf.setFontType("normal");
            pdf.text(45, 45, form.aduana.value);

            pdf.setFontType("bold");
            pdf.text(90, 45, 'ADMINISTRACIÓN ADUANA: ');
            pdf.setFontType("normal");
            pdf.text(130, 45, administracion);

            pdf.setDrawColor(0);
            pdf.setFillColor(200, 200, 200);
            pdf.roundedRect(20, 48, 176, 6, 1, 1, 'FD');
            pdf.setFontSize(8);
            pdf.setFontType("bold");
            pdf.text(22, 52, 'RUBRO 1. DATOS GENERALES');

            pdf.setFontSize(7);
            pdf.text(20, 60, 'N° MANIFIESTO EXPRESO: ');
            pdf.setFontType("normal");
            pdf.text(60, 60, form.anio_manif.value + '/' + form.aduana.value + '-' + form.nro_manif.value);

            pdf.setFontType("bold");
            pdf.text(90, 60, 'N° GUÍA AÉREA: ');
            pdf.setFontType("normal");
            pdf.text(115, 60, form.doc_embarque.value);

            pdf.roundedRect(20, 65, 176, 15, 1, 1);
            pdf.line(20, 71, 196, 71);
            pdf.line(80, 65, 80, 80);
            pdf.line(138, 65, 138, 80);

            pdf.setFontType("bold");
            pdf.text(22, 69, 'EMPRESA DE SERVICIO EXPRESO');
            pdf.setFontType("normal");
            pdf.text(22, 76, form.consignatario.value);

            pdf.setFontType("bold");
            pdf.text(82, 69, 'DIRECCIÓN: PROCEDENCIA Y DESTINO');
            pdf.setFontType("normal");
            pdf.text(82, 76, '');

            pdf.setFontType("bold");
            pdf.text(140, 69, 'FECHA DE SALIDA       FECHA DE LLEGADA');
            pdf.setFontType("normal");
            pdf.text(140, 76, '');
            pdf.text(170, 76, '');

            pdf.setDrawColor(0);
            pdf.setFillColor(200, 200, 200);
            pdf.roundedRect(20, 84, 176, 6, 1, 1, 'FD');
            pdf.setFontSize(8);
            pdf.setFontType("bold");
            pdf.text(22, 88, 'RUBRO 2. DATOS DEL ENVIO');

            pdf.roundedRect(20, 91, 176, 60, 1, 1);
            pdf.line(20, 97, 196, 97);
            pdf.line(27, 91, 27, 151);
            pdf.line(70, 91, 70, 151);
            pdf.line(110, 91, 110, 151);
            pdf.line(160, 91, 160, 151);

            pdf.setFontSize(7);
            pdf.setFontType("bold");
            pdf.text(22, 95, 'N°');
            pdf.setFontType("normal");
            pdf.text(22, 102, '1');

            pdf.setFontType("bold");
            pdf.text(29, 95, 'N° BULTOS');
            pdf.setFontType("normal");
            pdf.text(29, 102, '1');

            pdf.setFontType("bold");
            pdf.text(72, 95, 'PESO BRUTO EFECTIVO');
            pdf.setFontType("normal");
            pdf.text(72, 102, form.peso_real.value);

            pdf.setFontType("bold");
            pdf.text(112, 95, 'DESCRIPCIÓN');
            pdf.setFontType("normal");
            pdf.text(112, 102, form.descripcion.value);

            pdf.setFontType("bold");
            pdf.text(162, 95, 'OBSERVACIONES');
            pdf.setFontType("normal");
            pdf.text(162, 102, '');

            pdf.roundedRect(20, 152, 176, 12, 1, 1);
            pdf.setFontType("bold");
            pdf.text(22, 156, 'OBSERVACIONES: ');
            pdf.setFontType("normal");
            pdf.text(22, 160, form.observacion.value);

            pdf.text(20, 169, 'Mediante el presente formulario se autoriza la salida de la documentación descrita precedentemente sin el pago de tributos aduaneros.');

            pdf.roundedRect(20, 171, 176, 20, 1, 1);
            pdf.setFontSize(6);
            pdf.text(22, 186, 'Firma y sello funcionario de aduana');
            pdf.text(22, 189, 'AUTORIZACIÓN');
            pdf.text(82, 186, 'Firma y sello funcionario Courier');
            pdf.text(82, 189, 'PARTICIPACIÓN');
            pdf.text(138, 186, 'Firma y sello funcionario Concesionario');
            pdf.text(138, 189, 'PARTICIPACIÓN');

            pdf.roundedRect(20, 192, 30, 6, 1, 1);
            pdf.setFontSize(7);
            pdf.setFontType("bold");
            pdf.text(22, 196, 'FECHA: ');
            pdf.setFontType("normal");
            pdf.text(32, 196, Anb.datetime.now());
        }
        
        pdf.save('Constancia_de_entrega.pdf');
        
    }
</script>