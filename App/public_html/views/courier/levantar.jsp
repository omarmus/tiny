<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <i class="glyphicon glyphicon-send"></i> Levantar retención para: ${CourierForm.anio_manif}/${CourierForm.aduana}-${CourierForm.nro_manif} - ${CourierForm.doc_embarque}
        <div class="pull-right">
            <span class="label" style="color: crimson;">ESTADO:</span>
            <span class="label label-${CourierForm.estado == 'POR REVISAR' ? 'info' : CourierForm.estado == 'RETENIDO' ? 'danger' : 'warning'}">
                ${CourierForm.estado}
            </span>
        </div>
    </h4>
</div>
<html:form action="courierGuardarLevantar.do" styleId="form-levantar" method="post">
    <div class="modal-body">
        <%@ include file="datos_courier.jsp" %>
        <div class="alert alert-warning modal-details">
            <div class="form-group">
                <label>Peso Real: </label> 
                <strong class="form-control-static alert-danger">${CourierForm.peso_real} Kg.</strong>
            </div>
            <div class="form-group">
                <label>Peso Observado: </label> 
                <strong class="form-control-static alert-danger">${CourierForm.peso_observado} Kg.</strong>
            </div>
        </div>
        <fieldset>
            <legend>Manifiesto de descargo</legend>
            <div class="form-group text-left" style="width: 14%;">
                <label>Gestión <strong>*</strong></label> 
                <input type="text" name="des_gestion" id="des-gestion" class="manifiesto form-control integer required" maxlength="4" data-min="2000" data-max="2100"/>
            </div>
            <div class="form-group text-left" style="width: 15%;">
                <label>Aduana <strong>*</strong></label>
                <select name="des_aduana" id="des-aduana" class="manifiesto form-control integer required">
                    <option value="">Seleccione</option>
                    <option value="211">211</option>
                    <option value="311">311</option>
                    <option value="711">711</option>
                </select>
            </div>
            <div class="form-group text-left" style="width: 14%;">
                <label>Manifiesto <strong>*</strong></label> 
                <input type="text" name="des_manifiesto" id="des-manifiesto" class="manifiesto form-control integer required" maxlength="20" />
            </div>
            <div class="form-group text-left" style="width: 10%;">
                <label style="width: 100%;">&nbsp;</label> 
                <button class="btn btn-success" id="btn-docs" disabled="disabled" type="button">Verificar</button>
            </div>
            <div class="form-group text-left" style="width: 47%;">
                <label>Documento(s) de embarque <strong>*</strong></label> 
                <div class="form-control container-docs" id="container-docs"></div>
                <div id="error-docs"></div>
                <input type="hidden" id="items-docs" name="documentos">
            </div>
        </fieldset>
    </div>
    <div class="modal-footer"> 
        <button type="button" class="btn btn-default" data-dismiss="modal"> 
            <span class="glyphicon glyphicon-ban-circle"></span> Cancelar 
        </button>
        <button type="submit" class="btn btn-primary save-loading" disabled="disabled"> 
            <span> 
                <span class="glyphicon glyphicon-ok"></span> <span class="title-submit">Levantar retención</span> 
            </span> 
            <span> 
                <img src="img/loader.gif"> <span>Procesando</span> 
            </span> 
        </button>
    </div>
</html:form>
<script>
    var $gestion = $('#des-gestion'),
        $aduana = $('#des-aduana'),
        $manifiesto = $('#des-manifiesto'),
        $btn_docs = $('#btn-docs'),
        $container_docs = $('#container-docs'),
        $items_docs = $('#items-docs'),
        $error_docs = $('#error-docs'),
        documents = '',
        peso_obs = parseFloat($('#peso-obs').val()),
        peso_total = 0;

    $btn_docs.on('click', function() {
        $.get('courierListaGuias.do', {
            gestion : $gestion.val(),
            aduana : $aduana.val(),
            manifiesto : $manifiesto.val()
        }, function (response) {
            if (response.length) {
                var text = ''; 
                documents = '';
                peso_total = 0;
                for (var i = 0, l = response.length; i < l; i++) {
                    var id = response[i].id, 
                        peso = response[i].peso;
                    text += '<span class="label label-default btn-xs"><strong>Doc:</strong> ' + id + ' - <strong>Peso:</strong> ' + peso + 'Kg.</span>';
                    documents += id + ',';
                    peso_total += parseFloat(peso);
                }
                var peso = peso_obs - peso_total,
                    info = '<div class="alert alert-info alert-docs"><strong>Peso total de los docs. de embarque: </strong>' + peso_total + ' Kg. </div>';
                if (peso < 0) {
                    $error_docs.html(info + '<div class="alert alert-danger alert-docs">La suma de los docs. de embarque excede al peso observado.</div>');
                } else {
                    if (peso > 0) {
                        $error_docs.html(info + '<div class="alert alert-danger alert-docs">La suma de los docs. de embarque no es suficiente para realizar el descargo.</div>');
                    } else {
                        $error_docs.html(info + '<div class="alert alert-success alert-docs">La suma de los docs. es correcta para realizar el descargo.</div>');
                    }
                }
                $items_docs.val(documents.replace(/,+$/,''));
                $container_docs.html(text);
            } else {
                $items_docs.val('');
                Anb.alert('<div class="alert alert-danger">El manifiesto no tiene documentos de embarque, debe ingresar otro manifiesto.</div>', function () {
                    $manifiesto.focus();
                });
            }
        }, 'json');
    });

    $('#main-modal').on('keyup', 'input.manifiesto', function(e) {
        setData();
    }).on('change', 'select.manifiesto', function(e) {
        setData();
    });

    var setData = function () {
        var empty = Anb.filter.empty($gestion.val()) || Anb.filter.empty($aduana.val()) || Anb.filter.empty($manifiesto.val());
        $btn_docs[0].disabled = empty;
        $container_docs.html('');
        $items_docs.val('');
        $error_docs.html('');
    }

    $('#form-levantar').on('submit', function(e) {
        e.preventDefault();
        Anb.form.submit(this, 'courierGuardarLevantar.do', {
            validate : function (form) {
                if (Anb.filter.empty($items_docs.val())) {
                    Anb.alert('Debe verificar que el manifiesto de descargo tenga documentos de embarque.');
                    return false;
                }
                if ($error_docs.find('.alert-danger').length) {
                    Anb.alert('La suma de los documentos de embarque deben ser igual al peso observado.');
                    return false;   
                }
                Anb.form.submit(form, 'courierGuardarLevantar.do', {
                    validate : function (form) {
                        Anb.loading.show('Verificando peso real');
                        $.post('courierPesoReal.do', $(form).serialize(), function(response) {
                            Anb.loading.hide();
                            if (response == "OK") {
                                Anb.form.submit(form, 'courierGuardarLevantar.do', {
                                    success : function (data) {
                                        Anb.modal.update('courierDetalles.do?' + getPkGuia($('#pk')));
                                    }
                                });
                            } else {
                                Anb.alert("El peso real no concuerda con el manifiesto en SIDUNEA o el manifiesto ya fué descargado.");
                            }
                        });
                        return false;
                    }
                });
                return false;
            }
        });
    });
</script>
