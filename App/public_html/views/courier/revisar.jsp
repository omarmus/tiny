<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ include file="/helpers/form_helper.jsp" %>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <i class="glyphicon glyphicon-send"></i> Detalles de la Guía courier
        <div class="pull-right">
            <span class="label" style="color: crimson;">ESTADO:</span>
            <span class="label label-${CourierForm.estado == 'POR REVISAR' ? 'info' : CourierForm.estado == 'RETENIDO' ? 'danger' : 'warning'}">
                ${CourierForm.estado}
            </span>
        </div>
    </h4>
</div>
<div class="modal-body">
	<%@ include file="datos_courier.jsp" %>
</div>
<div class="modal-footer"> 
    <button type="button" class="btn btn-default" data-dismiss="modal"> 
        <span class="glyphicon glyphicon-ban-circle"></span> Cancelar 
    </button>
    <button type="button" class="btn btn-danger save-loading" id="retencion"> 
        <span> 
            <span class="glyphicon glyphicon-ok"></span> <span class="title-submit">Realizar retención</span> 
        </span> 
        <span> 
            <img src="img/loader.gif"> <span>Procesando</span> 
        </span> 
    </button>
    <button type="button" class="btn btn-success save-loading" id="autorizar"> 
        <span> 
            <span class="glyphicon glyphicon-ok"></span> <span class="title-submit">Autorizar retiro</span> 
        </span> 
        <span> 
            <img src="img/loader.gif"> <span>Procesando</span> 
        </span> 
    </button>
</div>

<script>
    var $pk = $('#pk');
    
    $('#retencion').on('click', function () {
        Anb.modal.update('courierRetencion.do?' + getPkGuia($pk), atras);
    });
    $('#autorizar').on('click', function () {
        Anb.modal.update('courierAutorizar.do?' + getPkGuia($pk), atras);
    });

    function atras () {
        $('#atras').on('click', function () {
            Anb.modal.update('courierDetalles.do?' + getPkGuia($pk));
        });
    }
</script>