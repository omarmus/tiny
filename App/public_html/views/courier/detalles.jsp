<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <i class="glyphicon glyphicon-send"></i> Detalles de la Guía courier
        <div class="pull-right">
            <span class="label" style="color: crimson;">ESTADO:</span>
            <span class="label label-${CourierForm.estado == 'POR REVISAR' ? 'info' : CourierForm.estado == 'RETENIDO' ? 'danger' : CourierForm.estado == 'POR AUTORIZAR' ? 'warning' : 'success'}">
                ${CourierForm.estado == 'IMPRIMIDO' ? 'CON CONSTANCIA DE ENTREGA' : CourierForm.estado}
            </span>
        </div>
    </h4>
</div>
<html:form action="courierGuardarAutorizar.do" method="post" onsubmit="return Anb.form.submit(this, 'courierGuardarAutorizar.do')">
    <div class="modal-body">
        <%@ include file="/views/courier/datos_courier.jsp" %>     
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"> 
            <span class="glyphicon glyphicon-ban-circle"></span> Cerrar 
        </button>
    </div>
</html:form>