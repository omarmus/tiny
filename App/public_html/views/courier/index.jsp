<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-table"></i> <strong>Lista de Guías Courier</strong> <sub>pendientes</sub></h3>
    </div>
    <div class="panel-body">
        <div>
            <button class="btn btn-default">Default</button>
            <button class="btn btn-primary">Primary</button>
            <button class="btn btn-warning">Warning</button>
            <button class="btn btn-danger">Danger</button>
            <button class="btn btn-success">Success</button>
            <button class="btn btn-info">Info</button>            
        </div>
        <table class="table table-striped table-hover" id="main-table">
            <thead>
                <tr>
                    <th class="edit">Estado</th>
                    <th>Manifiesto</th>
                    <th>Doc. embarque</th>
                    <th>Consignatario</th>
                    <th>Tipo Mercancía</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${couriers}" var="courier">
                <tr>
                    <td class="edit">
                        <button class="btn btn-${courier.estado == 'POR REVISAR' ? 'info' : courier.estado == 'RETENIDO' ? 'danger' : 'warning'} btn-sm view" 
                                type="button"
                                title="Editar guía"
                                data-fecha-partida="${courier.fecha_partida}"
                                data-aduana="${courier.aduana}"
                                data-nro-viaje="${courier.nro_viaje}"
                                data-doc-embarque="${courier.doc_embarque}">
                            <i class="glyphicon glyphicon-send"></i> ${courier.estado}
                        </button>
                    </td>
                    <td>${courier.anio_manif}/${courier.aduana}-${courier.nro_manif}</td>
                    <td>${courier.doc_embarque}</td>
                    <td>${courier.consignatario}</td>
                    <td class="text-center">
                        <span class="label label-${courier.tipo == 'SACA VERDE' ? 'success' : 'primary'}">${courier.tipo}</span>
                    </td>
                </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>

<script>
    var DT = null;

    $(document).ready(function () {
        DT = new Anb.datatable({
            filter: true,
            oLanguage: {"sSearch": '<i class="glyphicon glyphicon-search"></i> Buscar courier: '}
        });

        DT.$('.view').on('click', function() {           
            Anb.modal.show('courierDetalles.do?' + getPkGuia($(this)), 'lg');
        });
    });

    var getPkGuia = function ($el) {
        var pk = 'fecha-partida=' + $el.data('fecha-partida');
            pk += '&aduana=' + $el.data('aduana');
            pk += '&nro-viaje=' + $el.data('nro-viaje');
            pk += '&doc-embarque=' + $el.data('doc-embarque');
        return pk;
    }
</script>