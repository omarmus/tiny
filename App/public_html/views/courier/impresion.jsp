<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<div class="panel panel-default imprimir">
    <div class="panel-heading">
        <h3 class="panel-title"><strong>Impresión de Constancia de entrega Courier</strong></h3>
    </div>
    <div class="panel-body">
        <form id="form-levantar" method="post">
            <div class="form-group text-left" style="width: 15%;">
                <label>Gestión <strong>*</strong></label> 
                <input type="text" name="gestion" class="manifiesto form-control integer required" maxlength="4" data-min="2000" data-max="2100"/>
            </div>
            <div class="form-group text-left" style="width: 15%;">
                <label>Aduana <strong>*</strong></label>
                <select name="aduana" class="manifiesto form-control integer required">
                    <option value="">Seleccione</option>
                    <option value="211">211</option>
                    <option value="311">311</option>
                    <option value="711">711</option>
                </select>
            </div>
            <div class="form-group text-left" style="width: 15%;">
                <label>Manifiesto <strong>*</strong></label> 
                <input type="text" name="manifiesto" class="manifiesto form-control integer required" maxlength="20" />
            </div>
            <div class="form-group text-left" style="width: 20%;">
                <label>Doc. Embarque <strong>*</strong></label> 
                <input type="text" name="doc_embarque" class="manifiesto form-control integer required" maxlength="20" />
            </div>
            <div class="form-group text-left" style="width: 30%;">
                <label style="width: 100%;">&nbsp;</label>
                <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Buscar Courier</button>
                <button type="reset" id="limpiar" class="btn btn-default"><i class="glyphicon glyphicon-remove-circle"></i> Limpiar</button>
            </div>
        </form>
    </div>
</div>
<div id="container-guia"></div>

<script>
    $(document).ready(function () {
        var $form_levantar = $('#form-levantar'),
                $container_guia = $('#container-guia');
        $form_levantar.on('submit', function (e) {
            e.preventDefault();
            if (Anb.validate.run(this)) {
                Anb.loading.show('Buscando guía Courier');
                $.get('impresionBuscarGuia.do', $form_levantar.serialize(), function (response) {
                    Anb.loading.hide();
                    if (response.state && response.state === 'ERROR') {
                        Anb.message.error(response.data);
                        $container_guia.html('');
                    } else {
                        $container_guia.html(response);
                    }
                });
            }
        });

        $('#limpiar').on('click', function () {
            Anb.form.reset('#form-levantar');
            $container_guia.html('');
        });
    });
</script>