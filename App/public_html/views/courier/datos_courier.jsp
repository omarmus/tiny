<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<div class="alert alert-info modal-details">
    <div class="form-group">
        <label>Manifiesto: </label>
        <span class="form-control-static">${CourierForm.anio_manif}/${CourierForm.aduana}-${CourierForm.nro_manif}</span>
    </div>
    <div class="form-group">
        <label>Doc. de embarque: </label>
        <span class="form-control-static">${CourierForm.doc_embarque}</span>
    </div>
    <div class="form-group">
        <label>Consignatario: </label>
        <span class="form-control-static">${CourierForm.consignatario}</span>
    </div>
    <div class="form-group">
        <label>Doc. Consignatario: </label>
        <span class="form-control-static">${CourierForm.doc_consignatario}</span>
    </div>
    <div class="form-group">
        <label>Tipo mercancía: </label>
        <span class="form-control-static"><span class="label label-${CourierForm.tipo == 'SACA VERDE' ? 'success' : 'primary'}">${CourierForm.tipo}</span></span>
    </div>
    <div class="form-group" style="width: 25%;">
        <label>Peso: </label>
        <strong class="form-control-static alert-danger">${CourierForm.peso} Kg.</strong>
    </div>
    <div class="form-group" style="width: 25%;">
        <label>Cantidad: </label>
        <span class="form-control-static">${CourierForm.cantidad} unidad${CourierForm.cantidad > 1 ? 'es' : ''}</span>
    </div>
    <div class="form-group"style="width: 100%;">
        <label>Descripción mercancía: </label>
        <span class="form-control-static">${CourierForm.descripcion}</span>
    </div>
</div>
<input type="hidden" name="aduana" value="${CourierForm.aduana}"/>
<input type="hidden" name="nro_viaje" value="${CourierForm.nro_viaje}"/>
<input type="hidden" name="fecha_partida" value="${CourierForm.fecha_partida}"/>
<input type="hidden" name="doc_embarque" value="${CourierForm.doc_embarque}"/>
<input type="hidden" name="nro_linea" value="${CourierForm.nro_linea}"/>
<input type="hidden" name="anio_manif" value="${CourierForm.anio_manif}"/>
<input type="hidden" name="nro_manif" value="${CourierForm.nro_manif}"/>
<input type="hidden" name="fecha_manif" value="${CourierForm.fecha_manif}"/>
<input type="hidden" name="doc_consignatario" value="${CourierForm.doc_consignatario}"/>
<input type="hidden" name="consignatario" value="${CourierForm.consignatario}"/>
<input type="hidden" name="tipo" value="${CourierForm.tipo}"/>
<input type="hidden" name="cantidad" value="${CourierForm.cantidad}"/>
<input type="hidden" name="peso" value="${CourierForm.peso}"/>
<input type="hidden" name="descripcion" value="${CourierForm.descripcion}"/>

<c:if test="${CourierForm.estado != 'POR REVISAR'}">
    <input type="hidden" name="peso_real" value="${CourierForm.peso_real}"/>
    <input type="hidden" name="peso_observado" value="${CourierForm.peso_observado}" id="peso-obs" />
    <c:if test="${CourierForm.estado != 'POR AUTORIZAR'}">
        <input type="hidden" name="observacion" value="${CourierForm.observacion}"/>
    </c:if>
</c:if>

<input type="hidden" id="pk"
       data-fecha-partida="${CourierForm.fecha_partida}"
       data-aduana="${CourierForm.aduana}"
       data-nro-viaje="${CourierForm.nro_viaje}"
       data-doc-embarque="${CourierForm.doc_embarque}">

<c:if test="${view == 'YES'}">
<div class="alert alert-warning modal-details">
    <c:if test="${CourierForm.peso_real > 0}">
    <div class="form-group">
        <label>Peso real: </label>
        <span class="form-control-static">${CourierForm.peso_real}</span>
    </div>
    </c:if>
    <c:if test="${CourierForm.peso_observado > 0}">
    <div class="form-group">
        <label>Peso observado: </label>
        <span class="form-control-static">${CourierForm.peso_observado}</span>
    </div>
    </c:if>
    <div class="form-group" style="width: 100%;">
        <label>Observaciones: </label>
        <span class="form-control-static">${CourierForm.observacion}</span>
    </div>
</div>
</c:if>

<c:if test="${!empty descargos}">
    <h4><i class="glyphicon glyphicon-list-alt"></i> Documentos de descargo</h4>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th class="text-center">Nro. de viaje</th>
                <th class="text-center">Aduana</th>
                <th class="text-center">Fecha partida</th>
                <th class="text-center">Doc. de embarque</th>
                <th class="text-right">Peso</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${descargos}" var="descargo">
            <tr>
                <td class="text-center">${descargo.reg_voy_nber}</td>
                <td class="text-center">${descargo.reg_cuo}</td>
                <td class="text-center">${descargo.reg_dep_date}</td>
                <td class="text-center">${descargo.reg_bol_ref}</td>
                <td class="text-right">${descargo.reg_peso} Kg.</td>
            </tr>
            </c:forEach>
        </tbody>
    </table>
    
</c:if>