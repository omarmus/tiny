'use strict';

// Config Jquery
(function($) {
    $.ajaxSetup({
        dataFilter : function (data, type) {
            Anb.loading.hide();
            if (data == 'SESSION_EXPIRED' || data == 'ERROR_PERMISSION') {
                Anb.modal.hide();
                Anb.modal.hide("second-modal");
                $('.dataTables_processing').hide();
                if (data == 'SESSION_EXPIRED') {
                    Anb.$alert_modal.load('loginAjaxForm.do', function () {
                        Anb.$alert_modal.modal({
                            keyboard : false
                        }).modal('show')
                        .off('click.dismiss.bs.modal');
                    });
                    throw new Error('Session expired.');
                } else {
                    Anb.message.error("No tiene los permisos necesarios");
                    throw new Error('Error permission.');
                }
            }
            return data;
        },
        beforeSend : function () {
            if (Anb.loading.$container.css('display') == 'none') {
                Anb.loading.show();
            }
        }
    });
    //Datepicker
    $.fn.datepicker.dates['es'] = {
        days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
        daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
        daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        today: "Hoy"
    };
    //Datatable
    if ($.fn.dataTable) {
        $.extend( true, $.fn.dataTable.defaults, {
            "sDom" : "<'row'<'col-xs-12 col-md-4'><'col-xs-12 col-md-8 text-right dt-filter'fl>r>t<'row'<'col-xs-12 col-md-5'i><'col-xs-12 col-md-7'p>>", 
            "bFilter" : false, 
            "sPaginationType" : "full_numbers", 
            //"aoColumnDefs" : [{"bVisible" : false, "aTargets" : [0]}], 
            "aLengthMenu" : [[10, 25, 50,  - 1], [10, 25, 50, "Todos"]], 
            "oLanguage" : {
                "sSearch" : "Buscar",
                "sLengthMenu" : "Filas _MENU_", 
                "sZeroRecords" : "No existe registros.", 
                "sInfo" : "Mostrando _START_ al _END_ de _TOTAL_ registros", 
                "sInfgetoEmpty" : "Mostrando 0 al 0 de 0 registros", 
                "sProcessing" : "Cargando registros",
                "oPaginate" : {
                    "sNext" : "&raquo;", "sPrevious" : "&laquo;", "sFirst" : "Primero", "sLast" : "Último"
                }
            }
        });
    }

}(jQuery));

// Main Module
var Anb = (function (anb, $) {

    var tmpl_alert = $('#tmpl-alert').html();
    var tmpl_html = $('#tmpl-html').html();
    var $alert_modal = $("#alert-modal");
    var nano = function (template, data) {
        return template.replace(/\{([\w\.]*)\}/g, function (str, key) {
            var keys = key.split("."), v = data[keys.shift()];
            for (var i = 0, l = keys.length;i < l;i++)
                v = v[keys[i]];
            return (typeof v !== "undefined" && v !== null) ? v : "";
        });
    }

    var isIE = function () {
        if (navigator.appName == 'Microsoft Internet Explorer') {
            var re = new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})");
            if (re.exec(navigator.userAgent) != null) {
                return parseFloat( RegExp.$1 );
            }
        }
        return -1;
    }
    
    anb.types = ['text', 'textarea', 'file', 'password',  'email', 'search', 'number'];
    anb.$alert_modal = $alert_modal;
    anb.nano = nano;
    anb.ie = isIE();
    
    anb.toType = function (obj) {
        return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
    }
    
    anb.isJson = function (text) {
        return /^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
               replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
               replace(/(?:^|:|,)(?:\s*\[)+/g, ''));
    }
    
    anb.alert = function (message, callback_ok, title) {
        $alert_modal.html(nano(tmpl_alert, {
            title : title || 'Alerta', 
            message : message, 
            button_cancel : 'hide'
        })).modal();
        if (callback_ok) {
            $alert_modal.find('.modal-footer .btn-primary').on('click', callback_ok);
        }
    }

    anb.confirm = function (message, callback_ok, callback_cancel, title, label_ok, label_cancel) {
        $alert_modal.html(nano(tmpl_alert, {
            title : title || 'Confirmar', 
            message : message, 
            button_cancel : ''
        })).modal();

        var $btn_ok = $alert_modal.find('.modal-footer .btn-primary'),
            $btn_cancel = $alert_modal.find('.modal-footer .btn-default');

        $btn_ok.children('span').html(label_ok || 'Aceptar');
        $btn_cancel.children('span').html(label_cancel || 'Cancelar');
        if (callback_ok) {
            $btn_ok.on('click', callback_ok);
        }
        if (callback_cancel) {
            $btn_cancel.on('click', callback_cancel);
        }
    }

    anb.question = function (message, callback_yes, callback_no, title) {
        Anb.confirm(message, callback_yes, callback_no, title || 'Pregunta', 'Si', 'No');
    }
    
    anb.getParams = function (p, args) {
        var first = p.first.defaultValue,
            second = p.second.defaultValue;
            
        p.first.position = p.first.position || 0;
        p.second.position = p.second.position || 1;
        
        var type = Anb.toType(args[p.first.position]);
        if (type != 'undefined') {          
            if (type == p.first.type) {
                first = args[p.first.position];
                second = Anb.toType(args[p.second.position]) == p.second.type ? args[p.second.position] : p.second.defaultValue;
            } else {
                if (type == p.second.type) {
                    first = Anb.toType(args[p.second.position]) == p.first.type ? args[p.second.position] : p.first.defaultValue;
                    second = args[p.first.position];
                }
            }
        }
        return {
            first : first,
            second : second
        };
    }

    anb.print = function (html) 
    {
        var popup = window.open('', 'print', 'height=500,width=600, visible=false');
        /*optional stylesheet*/ //popup.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        popup.document.write(Anb.nano(tmpl_html, {contenido : html}));
        
        popup.document.close(); // necessary for IE >= 10
        popup.focus(); // necessary for IE >= 10

        popup.print();
        popup.close();

        return true;
    }
    
    anb.fullscreen = function () {
        if (!document.fullscreenElement &&    // alternative standard method
        !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
            if (document.documentElement.requestFullscreen) {
                document.documentElement.requestFullscreen();
            } else if (document.documentElement.msRequestFullscreen) {
                document.documentElement.msRequestFullscreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullscreen) {
                document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
        }
    }

    return anb;
}(Anb || {}, jQuery));

// Loading
Anb.loading = (function () {

    var $container = $('#loading-ajax');

    var loading = {};

    loading.$container = $container;
    
    loading.show = function (text) {
        $container.html(text || 'Cargando...').css({
            marginLeft : '-' + $container.outerWidth() / 2 + 'px'
        }).fadeIn(200);
    }

    loading.hide = function () {
        $container.fadeOut(200);
    }

    return loading;

}());

// Backup
Anb.backup = function (form, btn) {

    var backup = [];
    var $form = null;
    var $btn_submit = null;

    var change = function (obj) {
        for (var i = 0, l = backup.length; i < l; i++) {
            if (obj.name == backup[i].name) {
                backup[i].change = backup[i].checked ? obj.checked.toString() != backup[i].checked : obj.value != backup[i].value;
                break;
            }
        }
    }

    var isChange = function () {
        for (var i = 0, l = backup.length; i < l; i++) 
            if (backup[i].change) 
                return false;
        return true;
    }
    
    var setChanges = function (obj) { 
        change(obj);
        $btn_submit.prop({disabled: isChange()});
    }

    var init = function () {
        
        $form = $(form);
        $btn_submit = btn ? $(btn) : $form.find('[type=submit]');

        $btn_submit.prop('disabled', true);
        $form.find('textarea, [type=text], [type=file], [type=password], [type=email], [type=search], [type=number]').each(function () {
            backup.push({
                name   : this.name,
                value  : this.value,
                change : false
            });
        }).on('keyup', function () {
            setChanges(this);
        }).on('paste', function () {
            var el = this;
            setTimeout(function () {
                setChanges(el);
            }, 1);
        }).on('cut', function () {
            var el = this;
            setTimeout(function () {
                setChanges(el);
            }, 1);
        }).on('blur', function () {
            setChanges(this);
        });
        $form.find("select").each(function () {
            backup.push({
                name : this.name,
                value : this.value,
                change : false
            });
        }).on("change", function () {
            setChanges(this);
        }).on('blur', function () {
            setChanges(this);
        });
        $form.find('[type=checkbox]').each(function () {
            backup.push({
                name    : this.name,
                value   : this.value,
                checked : this.checked.toString(),
                change  : false
            });    
        }).on('click', function () {
            setChanges(this);
        }).on('blur', function () {
            setChanges(this);
        });

        var radios = [], 
            checkeds = [];
        var elements = $form[0].querySelectorAll('[type=radio]');
        for (var i = elements.length - 1; i >= 0; i--) {
            radios[elements[i].name] = '';
        }
        elements = $form[0].querySelectorAll('[type=radio]:checked');
        for (var i = elements.length - 1; i >= 0; i--) {
            checkeds[elements[i].name] = elements[i].value;
        }
        for (var name in radios) {
            backup.push({
                name   : name,
                value  : checkeds[name] || null,
                change : false
            });
        }
        $form.find('[type=radio]').on("click", function () {
            setChanges(this);
        }).on('blur', function () {
            setChanges(this);
        });
    }

    init ();

    return backup;
};

// Modal
Anb.modal = (function () {

    var main = 'main-modal';
    var second = 'second-modal';
    var modal = {};

    modal.main = main; 
   
    // Parameters url, size, callback
    modal.show = function (url) {
        var $modal = $('#' + main + ' .modal-content');        
        var parameters = Anb.getParams({
            first : {
                type : 'string',
                position : 1
            },
            second : {
                type : 'function',
                position : 2
            }
        }, arguments);
        var size = parameters.first, 
            callback = parameters.second,
            onfocus = typeof arguments[3] == 'undefined',
            show = typeof arguments[4] == 'undefined';
                     
        if (show) {
            if (size && size.length) {
                $modal.parent().addClass('modal-' + size);
            } else {
                $modal.parent().removeClass('modal-lg').removeClass('modal-sm');
            }
        }
        $.get(url, function (response) {
            if (response.state) {
                if (response.state == "ERROR") {
                    Anb.message.error(response.data || "Se produjo un error al momento de procesar su solicitud.");
                }
                return false;
            }
            $modal.html(response);
            $modal = $('#' + main);
            if (show) {
                $modal.modal(); //Show modal
            }
            var input = $modal.find('input[type=text], textarea').get(0);
            if (input && onfocus) {
                if (show) {
                    setTimeout(function () {input.focus()}, 500);
                } else {
                    input.focus();
                }
            }           

            Anb.load.render('#' + main);

            if ($modal.find("form").length) {
                var bk = new Anb.backup($modal.find("form")[0]);
            }

            if (callback) {
                setTimeout(function () {
                    callback.apply(window);
                }, 200);
            }
        });
    }

    modal.update = function (url, callback, focus) {
        modal.show(url, callback, null, focus, false);
    }
    
    // Parameters url, size, callback
    modal.secondShow = function (url) {
        
        var $modal = $('#' + second + ' .modal-content');        
        var parameters = Anb.getParams({
            first : {
                type : 'string',
                position : 1
            },
            second : {
                type : 'function',
                position : 2
            }
        }, arguments);
        var size = parameters.first, callback = parameters.second;
                     
        if (size && size.length) {
            $modal.parent().addClass('modal-' + size);
        } else {
            $modal.parent().removeClass('modal-lg').removeClass('modal-sm');
        }
        $.get(url, function (response) {
            if (response.state) {
                if (response.state == "ERROR") {
                    Anb.message.error(response.data || "Se produjo un error al momento de procesar su solicitud.");
                }
                return false;
            }
            $modal.html(response);
            $modal = $('#' + second);
            
            var input = $modal.modal().find('input[type=text], textarea').get(0);
            if (input) {
                setTimeout(function () {input.focus()}, 500);
            }
            if (callback) {
                setTimeout(function () {
                    callback.apply(window);
                }, 200);
            }
        });
    }

    modal.hide = function (id) {
        $('#' + (id || main)).modal('hide');
    }
    
    modal.secondHide = function () {
        $('#' + second).modal('hide');
    }

    return modal;

}());

// Filters
Anb.filter = (function () {

    var filter = {};

    filter.integer = function (e, value) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        if (e.keyCode == 86 && e.ctrlKey === true && value && !isNaN(value)) {
            return true;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    
    filter.decimal = function (e, value) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 188, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        if (e.keyCode == 86 && e.ctrlKey === true && value && !isNaN(value)) {
            return true;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }

    filter.removeTagHTML = function (text) {
        return text.replace(/<([^ >]+)[^>]*>.*?<\/\1>|<[^\/]+\/>/ig, "");
    }

    filter.empty = function (value) {
        return value == null || value.length == 0 || /^\s+$/.test(value);
    }

    filter.isEmail = function (value) {
        return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value); 
    }

    return filter;

}());

// Form
Anb.form = (function () {

    var counter = 0;
    var RESPONSE = ['OK', 'AJAX', 'CREATE', 'CREATED', 'CREATE-AJAX', 'CREATED-AJAX', 'UPDATE', 'UPDATE-AJAX', 'MAIL', 'CREATE-AND-MAIL', 'UPDATE-AND-MAIL' ];
    var CREATE = ['OK', 'CREATE', 'CREATED', 'CREATE-AJAX'];

    var form = {};

    form.integer = function (query) {
        $(query).on('keydown', function (e) {
            Anb.filter.integer(e, this.value);
        });
    }
    
    form.decimal = function (query) {
        $(query).on('keydown', function (e) {
            Anb.filter.decimal(e, this.value);
        });
    }

    form.maxlength = function (query) {
        counter = $('.counter').length;
        $((query || '') + ' [data-maxlength]').each(function () {
            var $this = $(this),
                maxlength = $this.data('maxlength');
            counter++;
            $this.prop('maxlength', maxlength).attr('data-counter', counter);
            $this.after('<span class="counter" id="counter-' + counter + '">' + (maxlength - this.value.length) + '</span>');  
        }).on('keyup', function() {
            var el = this,
                $this = $(this),
                maxlength = $this.data('maxlength'),
                length = el.value.length;
            $('#counter-' + $this.data('counter')).html(maxlength - length);
            if(Anb.ie > 0 && Anb.ie < 10 && length > maxlength) {
                el.value = el.value.substr(0, maxlength);
            }
        }).parent().css({overflow : 'hidden'});
    }

    /*
     * @param form
     * @param url
     * @param callback = { error : function , success: function, validate : function }
     */
    form.ajax = function () {

        var defaults = {
            error : null,
            success : null,
            validate : null
        }
  
        var params = Anb.getParams({
            first : {
                type : 'htmlformelement',
                defaultValue : 'main-table'
            },
            second : {
                type : 'string',
                defaultValue : ''
            }
        }, arguments);
        
        var form = params.first, 
            url = params.second, 
            callback = $.extend(defaults, arguments[2] || {});
            
        if(Anb.validate.run(form)) {
            if (Anb.toType(callback.validate) == 'function' && !callback.validate.apply(window, [form])) {
                return false;
            }
            var $form = $(form),
                data = null,
                $button = $form.find('[type=submit]').prop({disabled : true});
                
            $button.find('> span:first').hide();
            $button.find('> span:last').show();

            if ($form.find('[type=file]').length) {
                $.ajax({
                    url         : url,
                    data        : new FormData(form),
                    cache       : false,
                    contentType : false,
                    processData : false,
                    type        : 'POST',
                    success     : function(response){success(response);}
                });
            } else {
                $.post(url, $form.serialize(), function(response) {success(response); });
            }
        }

        var success = function (response) {
            if (response.state) {
                data = response.data;
                response = response.state;
            }
            if (RESPONSE.indexOf(response) != -1) {
                Anb.message.ok(CREATE.indexOf(response) != -1? (data.message || data || "¡El registro se ha creado correctamente!") : (data.message || data || "¡Actualizado correctamente!"), 500);
                if (response.indexOf('MAIL') == -1) {
                    Anb.message.mail();
                }
                if (Anb.toType(callback.success) == 'function') {
                    callback.success.apply(window, [data]);
                } else {
                    if (response.indexOf('AJAX') == -1) {
                        setTimeout(function () {
                            window.location = '';
                        }, 1500);
                    }
                }
            } else {
                if (response == "ERROR") {
                    Anb.message.error(data.message || data || "Se produjo un error al momento de procesar su solicitud.");
                } else {
                    Anb.message.error(response);
                }
                Anb.modal.hide();
                if (Anb.toType(callback.error) == 'function') {
                    callback.error.apply(window, [data]);
                }
            }
        }

        return false;
    }

    form.submit = function (form, callback) {
        var $form = $(form),
            $btn_submit = $form.find('[type=submit]'),
            form = $form[0],
            backup = new Anb.backup(form);

        $form.on('submit', function (e) {
            e.preventDefault();
            callback.apply(window, [form]);
        }).find('[data-form=clean]').on('click', function () {
            Anb.form.clean(form);
            $btn_submit.prop({disabled : true});            
        });
        $form.find('[type=reset]').on('click', function () {
            Anb.form.cleanErrors(form);
            $btn_submit.prop({disabled : true});
            var elements = form.querySelectorAll('textarea[maxLength]');
            setTimeout(function () {
                for (var i=0, l = elements.length; i <  l; i++) {
                    elements[i].nextSibling.innerHTML = elements[i].maxLength - elements[i].value.length;
                }          
            }, 100);
        });;
    }
    
    form.clean = function (form) {
        Anb.form.cleanErrors(form);
        Anb.form.cleanData(form);
    }

    form.cleanErrors = function (form) {
        var $form = $(form);
        $form.find('.input-error').hide();
        $form.find('.form-group').removeClass('has-feedback').removeClass('has-error').removeClass('has-success');
        $form.find('.form-group .glyphicon-remove').removeClass('.glyphicon-remove');
    }
        
    form.cleanData = function (form) {
        var elements = form.elements;  
        for (var i in elements) {
            var el = elements[i],
                type = el.type;

            if ($.inArray(type, Anb.types) != -1) {
               el.value = "";
                if (type == 'textarea' && el.maxLength) {
                    el.nextSibling.innerHTML = el.maxLength;
                }
            } else if (type == 'checkbox' || type == 'radio') {
               el.checked = false;
            } else if (type == 'select-one' || type == 'select-multiple') {
               el.selectedIndex = 0;
            }
        }
    }

    return form;

}());

// Validate
Anb.validate = (function () {

    var messages = {
        required               : 'El campo <em>{0}</em> es requerido.',
        email                  : 'El campo <em>{0}</em> no es un email válido.',
        nit                    : 'El campo <em>{0}</em> no es un NIT válido.',
        date                   : 'El campo <em>{0}</em> no es una fecha válida.',
        date_less_than         : 'El campo <em>{0}</em> debe contener una fecha <em>menor</em> a la fecha actual.',
        date_greater_than      : 'El campo <em>{0}</em> debe contener una fecha <em>mayor</em> a la fecha actual.',
        data_date_less_than    : 'El campo <em>{0}</em> debe contener una fecha <em>no menor</em> a <em>{1}</em> días.',        
        data_date_greater_than : 'El campo <em>{0}</em> debe contener una fecha <em>no mayor</em> a <em>{1}</em> días.',
        data_minlength         : 'El campo <em>{0}</em> debe contener un mínimo de <em>{1}</em> caracteres.',
        data_matches	       : 'El campo <em>{0}</em> no concuerda con el campo <em>{1}</em>.</div>'
    }

    var isNit = {
        permutations : function (x, y) {
            var v = ["0123456789", "1576283094", "5803796142", "8916043527", "9453126870", "4286573901", "2793806415", "7046913258"];
            if (v[x]) {
                return parseInt(v[x][y]);
            }
            return -1;
        },

        verhoeff : function (x, y) {
            var v = ["0123456789", "1234067895", "2340178956", "3401289567", "4012395678", "5987604321", "6598710432", "7659821043", "8765932104", "9876543210"]; 
            if (v[x]) {
                return parseInt(v[x][y]);
            }
            return -1;
        },

        evaluate : function (value) {
            var check = 0, x, y, z;
            for (var i = 0, l = value.length; i < l; i++) {
                x = i % 8;
                y = parseInt(value[l - i - 1]);
                z = permutations(x, y);
                check = verhoeff(check, z);
            }

            return check == 0;
        }
    }
    
    var getMessage = function ($el, data) {
        var value = '', el = $el[0];
        if (el.getAttribute('data-' + data)) {
            value = el.getAttribute('data-' + data);
            if (data == 'matches') {
                var $elem = $(el.form[value]),
                    $label = $elem.prev('label');
                if ($label.length == 0) {
                    $label = $elem.parent().prev('label');
                }
                value = $label.length ? $label[0].innerHTML.replace(/:/gi, '') : value;
            }
        }
        return value;
    }

    var renderError = function (message, $el) {
        var type = $el[0].type, 
            datepicker = $el.hasClass('datepicker-input'),
            parent = type == 'checkbox' || type == 'radio' || datepicker, 
            counter = $el[0].getAttribute('data-maxlength'),
            $error = parent ? $el.parent().nextAll('.input-' + message) : $el.nextAll('.input-' + message);
        if ($error.length) {
            $error.show();
        } else {
            var $elem = parent ? $el.parent() : $el,
                $label = $elem.prev('label');
                if ($label.length == 0) {
                    $label = $elem.parent().prev('label');
                }
            var data = message.replace(/data-/gi, ''),
                msg = $el.data('label-' + message) 
                   || $el.data('label') 
                   || messages[message.replace(/-/gi, '_')].replace(/\{0\}/gi, $label.length ? $label[0].innerHTML.replace(/:/gi, '') : '').replace(/\{1\}/gi, getMessage($el, data)),
                divError = '<div class="input-error input-' + message +'">' + msg + '</div>';
            if (counter) {
                $elem.next().after(divError);
            } else {
                $elem.after(divError);
            }
        }
        $el.closest('.form-group').addClass('has-error');
        if (!datepicker) {
            $el.focus();
        }
    }

    var removeError = function (el, class_name) {
        var $el = $(el).closest('.form-group');
        $el.removeClass('has-error');
        $el.find(class_name || '.input-required').fadeOut();
    }

    var addRuleText = function (form, rule, types, fn_validate, data) {
        var elements = form.querySelectorAll('input' + (data ? '[' + rule + ']' : '.' + rule)),
            valid = true;
        if (elements.length) {
            $(form).on('keyup', data ? '[' + rule + ']' : '.' + rule, function () {
                removeError(this, '.input-' + rule);
            });
            for (var i = elements.length - 1; i >= 0; i--) {
                var el = elements[i];
                if (el.className.indexOf('required') != -1 || el.parentNode.className.indexOf('required') != -1) {
                    data = el.getAttribute(rule) || false;
                    if ((Anb.toType(types) == 'array' ? types.indexOf(el.type) != -1 : el.type == types) && !(data ? fn_validate(el.value, data, form) : fn_validate(el.value))) {
                        renderError(rule, $(el));
                        valid = false;
                    }
                }
            }
        }
        return valid;
    }

    var required = function (form) {
        var elements = $(form).find('.required:visible'),
            valid = true,
            radios = [];
        if (elements.length) {
            $(form).on('keyup', 'input.required, textarea.required', function () {
                removeError(this);
            });
            $(form).on('change', 'select.required', function () {
                removeError(this);
            });
            $(form).on('click', '[type=checkbox].required, [type=radio].required', function () {
                removeError(this);
            });
            $(form).on('click', '[type=radio].required', function () {
                removeError(this);
            });            
            for (var i = elements.length - 1; i >= 0; i--) {
                var el = elements[i],
                    $el = $(el),
                    type = el.type;
                if (($.inArray(type, Anb.types) != -1 && Anb.filter.empty(el.value)) ||
                    (type == 'select-one' && (el.selectedIndex == null || el.selectedIndex == 0)) ||
                    (type == 'checkbox' && !el.checked)) {
                    renderError('required', $el);
                    valid = false;
                }
                if (type == 'radio') {
                    radios[el.name] = el.name;
                }
                if ($el.hasClass('ms-ctn') && $el.find('.ms-sel-item').length == 0) {
                    $el.addClass('ms-inv');
                    renderError('required', $el);
                    valid = false;
                }
                if ($el.hasClass('input-daterange')) {
                    var $inputs = $el.find('input');
                    if (Anb.filter.empty($inputs[0].value) && Anb.filter.empty($inputs[1].value)) {
                        renderError('required', $el);
                        valid = false;    
                    }
                }
            }
            for (var name in radios) {
                var options = form.querySelectorAll('[name=' + name + ']'),
                    checked = false,
                    label = null;
                for(var i = 0, l = options.length; i < l; i++) {    
                    if(options[i].checked) {
                        checked = true;
                        break;
                    }
                    if (options[i].getAttribute('data-label')) {
                        label = options[i].getAttribute('data-label');
                    }
                }
                if (label) {
                    options[l-1].setAttribute('data-label', label);
                }
                if(!checked) {
                    renderError('required', $(options[l-1]));
                    valid = false;
                }
            }
        }
        return valid;
    }

    var email = function (form) {
        return addRuleText(form, 'email', ['text', 'email'], Anb.filter.isEmail);
    }

    var nit = function (form) {
        return addRuleText(form, 'nit', ['text', 'number'], Anb.validate.isNit);
    }
    
    var matches = function (form) {
        return addRuleText(form, 'data-matches', ['text', 'email', 'password', 'number'], Anb.validate.equals, true);
    }

    var date = function (form) {
        return addRuleText(form, 'date', ['text', 'date'], Anb.datetime.isDate);
    }

    var dateLessThan = function (form) {
        return addRuleText(form, 'date-less-than', ['text', 'date'], Anb.datetime.isLessThan);
    }

    var dateLessThanDays = function (form) {
        return addRuleText(form, 'data-date-less-than', ['text', 'date'], Anb.datetime.isLessThanDays, true);
    }

    var dateGreaterThan = function (form) {
        return addRuleText(form, 'date-greater-than', ['text', 'date'], Anb.datetime.isGreaterThan);
    }

    var dateGreaterThanDays = function (form) {
        return addRuleText(form, 'data-date-greater-than', ['text', 'date'], Anb.datetime.isGreaterThanDays, true);
    }
    
    var minlength = function (form) {
        return addRuleText(form, 'data-minlength', ['text', 'number'], Anb.validate.minlength, true);
    }

    var validate = {};

    validate.run = function (form) {
           
        var $form = $(form),
            valid = true;
        if (Anb.toType(form) == 'string') {
            form = $form[0];
        }
        var $button = $form.find('[type=submit]').prop({disabled : true});

        Anb.validate.clean(form);
        
        valid = minlength(form) && valid;
        valid = dateLessThan(form) && valid;
        valid = dateLessThanDays(form) && valid;
        valid = dateGreaterThan(form) && valid;
        valid = dateGreaterThanDays(form) && valid;
        valid = date(form) && valid;
        valid = email(form) && valid;
        valid = nit(form) && valid;
        valid = matches(form) && valid;
        valid = required(form) && valid;

        $button[0].disabled = false;
        return valid;
    }

    validate.isNit = function (value) {
        return isNit.evaluate(value);
    }

    validate.minlength = function (value, minlength) {
        return value.length >= minlength;
    }
    
    validate.equals = function (value, data, form) {
        return value == form[data].value;
    }
    
    validate.removeError = removeError;
    
    validate.clean = function (form) {
        var elements = form.elements;  
        for (var i in elements) {
            if (Anb.types.indexOf(elements[i].type) != -1) {
               elements[i].value = $.trim(elements[i].value);
            }
        }
    }

    return validate;

}());

// Growl message
Anb.message = (function () {

    var $growls = $('#anb-messages');
    var tmpl_growl = $('#anb-message-tmpl').html();
    
    var message = {};
    
    var render = function (data) {
        var $growl = $(Anb.nano(tmpl_growl, data));
        $growls.append($growl);
        $growl.animate({opacity : .95}, function () {
            var stop = false;
            $growl.find('.close').on('click', function () {
                $growl.off('mouseover').off('mouseout');
                hide($growl);
            });
            $growl.on('mouseover', function () {
                stop = true;
                $growl.stop().css({opacity : .95});    
            });
            $growl.on('mouseout', function () {
                stop = false;
                setTimeout(function () {
                    if (!stop) {
                        hide($growl);
                    }
                }, data.delay || delay);
            });
            setTimeout(function () {
                if (!stop) {
                    hide($growl);
                }
            }, data.delay || delay);
        });
        
        if (data.callback) {
            data.callback.apply(window, [$growl[0]]);
        }
    }
    
    var hide = function ($growl) {
        $growl.fadeOut(1500, function () {
            setTimeout(function () {
                $growl.remove();
            }, 1000);
        });
    }

    var getParams = function (args) {
        var params = Anb.getParams({
            first : {
                type : 'function',
                position : 1
            },
            second : {
                type : 'number',
                position : 2
            }
        }, args);
        return {
            callback : params.first,
            delay : params.second
        }
    }
     
    // arguments : msg, callback, delay  
    message.error = function (msg) {
        var args = getParams(arguments);
        setTimeout(function () {
            render({
                type : 'error', 
                title : '¡Error!', 
                message : msg || 'Ha ocurrido un error.', 
                delay : 5000,
                icon : 'fa-times',
                callback : args.callback || null
            });
        }, args.delay || 0);
    }
    
    message.ok = function (msg) {
        var args = getParams(arguments);
        setTimeout(function () {
            render({
                type : 'success', 
                title : '¡Correcto!', 
                message : msg || 'La operación se realizó correctamente.', 
                delay : 5000,
                icon : 'fa-check',
                callback : args.callback || null
            });
        }, args.delay || 0);
    }

    message.mail = function (msg) {
        var args = getParams(arguments);
        setTimeout(function () {
            render({
                type : 'info', 
                title : '¡Correo!', 
                message : msg || 'Correo electrónico enviado.', 
                delay : 5000,
                icon : 'fa-envelope-o',
                callback : args.callback || null
            });
        }, args.delay || 0);
    }
    
    message.notification = function (msg) {
        var args = getParams(arguments);
        setTimeout(function () {
            render({
                type : 'warning', 
                title : '¡Notificación!', 
                message : msg || 'Hay nuevas notificaciones.', 
                delay : 5000,
                icon : 'fa-exclamation-triangle',
                callback : args.callback || null
            });
        }, args.delay || 0);
    }

    return message;

}());

// Datatable
Anb.datatable = function () {

    var oTable = null,
        selected = [],
        settings = {deferRender : true},
        $table = null,
        $checkAll = null;

    var checkbox = {
        fnInitComplete : function () {
            var that = this, firstTH = $(that).find('th')[0], th = document.createElement('th');
            th.innerHTML = "<i></i>";
            th.className = "check-all";
            th.onclick = function () {
                var toggle = (that.$('tr').length == that.$('tr.active').length ? 'remove' : 'add') + 'Class';
                $(firstTH).parent()[toggle]('all-selected');
                that.$('tr')[toggle]('active');
                var now = that.$('tr.active').length;
                $($(that).parent().find('.row .col-md-9')[0]).html(now ? '<span class="label label-warning"><strong>' + now + '</strong> ' + "Fila" + (now > 1 ? 's' : '') + ' ' + 'Seleccionada' + (now > 1 ? 's' : '') + '.</span>' : '');
            }
            $(firstTH).parent().prepend(th);
            if (that.$('tr').length == 0) {
                $(that).find('td').prop('colspan', $(that).find('td').prop('colspan') + 1);
            }
            that.$('tr').each(function () {
                var firstTD = this;
                var td = document.createElement('td');
                td.innerHTML = "<i></i>";
                td.className = "check-item";
                td.onclick = function () {
                    $(firstTD).toggleClass('active');
                    var now = that.$('tr.active').length;
                    $(firstTH).parent()[(that.$('tr').length == now ? 'add' : 'remove') + 'Class']('all-selected');
                    $($(that).parent().find('.row .col-md-9')[0]).html(now ? '<span class="label label-warning"><strong>' + now + '</strong> Fila' + (now > 1 ? 's' : '') + ' Seleccionada' + (now > 1 ? 's' : '') + '.</span>' : '');
                }
                $(firstTD).prepend(td);
            })
        }
    };
    
    // Set id_table and options   
    var parameters = Anb.getParams({
        first : {
            type : 'string',
            defaultValue : 'main-table'
        },
        second : {
            type : 'object',
            defaultValue : {}
        }
    }, arguments);
    var id_table = parameters.first, options = parameters.second;
       
    var init = function () {
        if (options.ajax) {
            settings = $.extend({}, settings,{
                "processing" : true,
                "serverSide" : true,
                "rowCallback": function( row, data ) {
                    if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                        $(row).addClass('active');
                    }
                 },
                "aoColumnDefs" : [
                    {"bVisible": true, "aTargets": [ 0 ]}
                ]
            });
            options.checkbox = false;
        }
        if (typeof options.checkbox !== 'undefined' && options.checkbox === true) {
            settings = $.extend({}, settings, checkbox);
            delete options.checkbox;
        }
        if (options.noSortable) {
            settings = $.extend({}, settings, {
                "aoColumnDefs" : [
                    {"bVisible": false, "aTargets": [ 0 ]}, 
                    {"bSortable": false, "aTargets": options.noSortable }
                ]
            });
            delete options.noSortable;
        }
        
        settings = $.extend({}, settings, options);
        $table = $('#' + id_table);
        oTable = $table.dataTable(settings);
        
        if (options.ajax) {
            $checkAll = $table.find(".check-all");
            $checkAll.on('click', function () {
                var toggle = ($checkAll.parent().hasClass("all-selected") ? 'remove' : 'add') + 'Class';
                if (toggle == "addClass") {
                    Anb.loading.show("Seleccionando los registros, espere por favor");
                    $.get(options.ajax, {all : true}, function(response) {
                        selected = response;
                    }, "json");
                } else {
                    selected = [];
                }
                $checkAll.parent()[toggle]('all-selected');
                oTable.$('tr')[toggle]('active');
                var now = toggle == 'addClass' ? oTable.$('table').dataTableSettings[0]._iRecordsTotal : 0;
                $($table.parent().find('.row .col-md-9')[0]).html(now ? '<span class="label label-warning"><strong>' + now + '</strong> ' + "Fila" + (now > 1 ? 's' : '') + ' ' + 'Seleccionada' + (now > 1 ? 's' : '') + '.</span>' : '');
            });
            $table.find('tbody').on( 'click', 'tr td.check-item', function () {           
                var id = this.parentNode.id;
                var index = $.inArray(id, selected);
                index === -1 ? selected.push(id) : selected.splice(index, 1);
                var now = selected.length;
                var total = oTable.$('table').dataTableSettings[0]._iRecordsTotal;
                $checkAll.parent()[(total == now ? 'add' : 'remove') + 'Class']('all-selected');
                $($table.parent().find('.row .col-md-9')[0]).html(now ? '<span class="label label-warning"><strong>' + now + '</strong> Fila' + (now > 1 ? 's' : '') + ' Seleccionada' + (now > 1 ? 's' : '') + '.</span>' : '');
                $(this).parent().toggleClass('active');
            });
        }
        var $tableContainer = $('#' + id_table + '_wrapper');
        $tableContainer.find('input, select').addClass('form-control');
        $tableContainer.find('.dataTables_filter input').attr('placeholder', options.filterPlaceholder || 'Escriba su búsqueda aquí...')
    }
    
    init();
    
    var delete_row = function (obj, url, refresh) {
        Anb.confirm("¿Deséa eliminar el registro?", function () {
            Anb.loading.show("Eliminando registro");
            $.post(url, function (response) {
                if (response == 'DELETE') {
                    Anb.message.ok("¡Registro eliminado!");
                    oTable.fnDeleteRow( $(obj).parents('tr') )
                    if (refresh) {
                        setTimeout(function () {
                            window.location = '';
                        }, 500);
                    }
                } else {
                    Anb.message.error(response);
                    Anb.loading.show("Recargando página");
                    setTimeout(function () {
                        window.location = '';
                    }, 4000);
                }
            });
        });
    }

    var datatable = {};
    
    datatable.delete_row = delete_row;
    
    datatable.selected = function () {
         var pks = [];
        if (options.ajax) {
            for (var i = 0, l = selected.length; i < l; i++) {
                pks.push(selected[i].split("_")[1]);
            }
        } else {
            oTable.$('tr.active').each(function () {
                pks.push(oTable.fnGetData(this)[0]);
            });
        }
        return pks;
    }
    
    datatable.btnEdit = function (url) {
        var parameters = Anb.getParams({
            first : {
                type : 'string',
                position : 1
            },
            second : {
                type : 'function',
                position : 2
            }
        }, arguments);
        var size = parameters.first, callback_function = parameters.second;
        oTable.$('.btn-edit').on('click', function (e) {
            e.preventDefault();
            Anb.modal.show(url + '?id=' + $(this).data('role'), size || callback_function, callback_function && size ? callback_function : size, arguments[3]);
        });
    }
    
    datatable.btnDelete = function (url, refresh) {
        oTable.$('.btn-delete').on('click', function (e) {
            e.preventDefault();
            delete_row(this, url + '?id=' + $(this).data('role'), refresh);
        });
    }
    
    datatable.btnView = function (search, url) {
        var parameters = Anb.getParams({
            first : {
                type : 'string',
                position : 2
            },
            second : {
                type : 'function',
                position : 3
            }
        }, arguments);
        var size = parameters.first, callback_function = parameters.second;
        oTable.$(search).on('click', function (e) {
            e.preventDefault();
            Anb.modal.show(url + '?id=' + $(this).data('role'), size || callback_function, callback_function && size ? callback_function : size);
        });
    }
    
    datatable.btnViewSecond = function (query, url) {
        var parameters = Anb.getParams({
            first : {
                type : 'string',
                position : 2
            },
            second : {
                type : 'function',
                position : 3
            }
        }, arguments);
        var size = parameters.first, callback_function = parameters.second;
        oTable.$(query).on('click', function (e) {
            e.preventDefault();
            Anb.modal.secondShow(url + '?id=' + $(this).data('role'), size || callback_function, callback_function && size ? callback_function : size);
        });
    }
    
    datatable.$ = function (query) {
        return oTable.$(query);
    }

    return datatable;

};

// Autocomplete
Anb.autocomplete = function (query, settings) {
    var defaults = {}, data = {},
        $el = $(query), 
        attributes = $el[0].attributes,
        classes = $el[0].className.split(/\s+/),
        ms = null; // object MagicSuggest

    var init = function () {
        for (var i = 0, l = attributes.length; i < l; i++) {
            var value = attributes[i].value;
            data[attributes[i].name.replace(/data-/gi, '')] = value === 'true' || (value === 'false' ? false : value);
        }
        for (var i = 0, l = classes.length; i < l; i++) {
            data[classes[i]] = true;
            settings.cls = (settings.cls || '') + ' ' + classes[i];
        }
        settings = $.extend(defaults, data, settings || {});
        if (settings.multiple) {
            settings.hideTrigger = true;
        }
        if (typeof settings.search != 'undefined' && !settings.search) {
            settings.cls = (settings.cls || '') + ' no-search';
        }
        ms = $el.magicSuggest(settings);
        if (settings.integer) {
            $(ms).on('keydown', function (e,m,v) {
                Anb.filter.integer(v);
            });
        }
        if (settings.decimal) {
            $(ms).on('keydown', function (e,m,v) {
                Anb.filter.decimal(v);
            });
        }
        if (settings.required) {
            $(ms).on('selectionchange', function(e,m) {
                var $el = $(query);
                $el.closest('div.form-group').removeClass('has-error').addClass('has-success');
                $el.nextAll('.input-required').fadeOut();
            });    
        }
    }

    init();
    
    ms.disabled = function (bool) {
        bool ? ms.disable() : ms.enable();
    }

    return ms;

};

// Array
Anb.array = (function () {

    var array = {};
    
    array.replace = function (text, find, replace) {
        for (var i = 0, l = find.length, regex; i < l; i++) {
            regex = new RegExp(find[i], "g");
            text = text.replace(regex, replace[i]);
        }
        return text;
    };

    return array;

}());


// Datetime
Anb.datetime = (function () {

    var formatDate = 'dd/MM/YYYY',
        months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        days = ["dom", "lun", "mar", "mié" , "jue", "vie" , "sáb"],
        dayslong = ["Domingo", "Lunes", "Martes", "Miércoles" , "Jueves", "Viernes" , "Sábado"];
    var tmpl_date = $('#tmpl-date').html();
    var datetime = {};

    datetime.isDate = function (date) {
        if (Anb.toType(date) == 'string') {
            date = Anb.datetime.convert(date);
        }
        return Anb.datetime.isValid(date);
    }

    datetime.isValid = function (date) {
        if (Anb.toType(date) != 'date')
            return false;
        return !isNaN(date.getTime());
    }

    datetime.isLessThan = function (date) {
        return Anb.datetime.milliseconds(date) < (new Date()).getTime();
    }

    datetime.isLessThanDays = function (date, data) {
        return Anb.datetime.milliseconds(date) > Anb.datetime.subtractDays(new Date(), data);
    }

    datetime.isGreaterThan = function (date) {
        return Anb.datetime.milliseconds(date) > (new Date()).getTime();
    }

    datetime.isGreaterThanDays = function (date, data) {
        return Anb.datetime.milliseconds(date) < Anb.datetime.addDays(new Date(), data);
    }

    datetime.addDays = function (date, days) {
        return Anb.datetime.milliseconds(date) + (days || 0)*24*60*60*1000;
    }

    datetime.subtractDays = function (date, days) {
        return Anb.datetime.milliseconds(date) - (days || 0)*24*60*60*1000;
    }

    datetime.diff = function (date2, date1) {
        return Anb.datetime.milliseconds(date2) - Anb.datetime.milliseconds(date1);
    }

    datetime.milliseconds = function (date) {
        if (Anb.toType(date) == 'string') {
            date = Anb.datetime.convert(date);
        }
        return date.getTime();
    }

    datetime.convert = function (date) {
        date = date.split('/');
        return formatDate == 'dd/MM/YYYY' ? new Date(date[2], date[1]-1, date[0]) : 
               formatDate == 'MM/dd/YYYY' ? new Date(date[2], date[0]-1, date[1]) : 
               new Date(date[0], date[1]-1, date[2]);
    }

    datetime.setFormatDate = function (format) {
        formatDate = format;
    }

    datetime.dateRange = function (query, settings) {
        var defaults = {
            format: formatDate.toLowerCase(),
            language: "es",
            todayHighlight: true,
            autoclose: true
        };
        settings = $.extend(defaults, settings || {});
        $(query).each(function () {
            var $el = $(this),
                el = this,
                $btn_submit = $(el.form).find("[type=submit]"),
                backup = el.value;

            if ($el.hasClass('date-less-than')) {
                settings.endDate = Anb.datetime.now();
            }
            if ($el.hasClass('date-greater-than')) {
                settings.startDate = Anb.datetime.now();
            }
            if (el.hasAttribute('data-date-less-than')) {
                settings.startDate = Anb.datetime.replace(Anb.datetime.subtractDays(new Date(), el.getAttribute('data-date-less-than')));
            }
            if (el.hasAttribute('data-date-greater-than')) {
                settings.endDate = Anb.datetime.replace(Anb.datetime.addDays(new Date(), el.getAttribute('data-date-greater-than')));
            }
            $el.find('input').attr('maxLength',10).attr('autocomplete','off');

            $el.datepicker(settings).on('changeDate', function () {
                if ($el.hasClass('required')) {
                    Anb.validate.removeError(this, '.input-error');
                }
                $btn_submit.prop('disabled', $el[0].value == backup);
            });
        });
    }
    
    datetime.date = function (query, settings) {
        var defaults = {
            format: formatDate.toLowerCase(),
            language: "es",
            todayHighlight: true,
            autoclose: true
        };
        settings = $.extend(defaults, settings || {});
        
        $(query).each(function () {
            var $el = $(this),
                el = this,
                $tmpl = $(tmpl_date),
                $btn_submit = $(el.form).find("[type=submit]"),
                backup = this.value;

            $tmpl.insertAfter($el);
            $el.prependTo($tmpl);
            $el.removeClass('datepicker').addClass('datepicker-input');
            if ($el.hasClass('date-less-than')) {
                settings.endDate = Anb.datetime.now();
            }
            if ($el.hasClass('date-greater-than')) {
                settings.startDate = Anb.datetime.now();
            }
            if (el.hasAttribute('data-date-less-than')) {
                settings.startDate = Anb.datetime.replace(Anb.datetime.subtractDays(new Date(), el.getAttribute('data-date-less-than')));
            }
            if (el.hasAttribute('data-date-greater-than')) {
                settings.endDate = Anb.datetime.replace(Anb.datetime.addDays(new Date(), el.getAttribute('data-date-greater-than')));
            }
            el.maxLength = 10;
            el.setAttribute('autocomplete', 'off');
            $tmpl.datepicker(settings).on('changeDate', function () {                
                if ($el.hasClass('required')) {
                    Anb.validate.removeError(this, '.input-error');
                }
                $btn_submit.prop('disabled', $el[0].value == backup);
            });
            delete settings['endDate'];
            delete settings['startDate'];
        });
    }
    
    datetime.now = function (format) {
        return Anb.datetime.replace(new Date(), format || formatDate);
    }

    datetime.dateLiteral = function (date) {
        return datetime.format(date, 'dd de MMM del YYYY');
    }
    
    datetime.timeLiteral = function (date) {
        return datetime.format(date, 'HH:mm');
    }

    datetime.datetimeLiteral = function (date) {
        return datetime.format(date, 'dddd dd de MMM del YYYY a la(s) HH:mm');
    }
    
    datetime.format = function (date, format) {
        var d = new Date(date);
        if (Anb.datetime.isDate(d)) {
            return Anb.datetime.replace(d, format || formatDate);
        }
        return date;
    }

    datetime.standar = function (date, format) {
        var type = Anb.toType(date);
        if (type == 'date') {
            return date;
        }
        if (type == 'string') {
            format = format || formatDate;
            var separator = date.indexOf('/') != -1 ? '/' : '-';
            date = date.split(separator);
            if (format == 'dd/MM/YYYY' || format == 'dd-MM-YYYY') {
                date = date[1] + separator + date[0] + separator + date[2];    
            }
            if (format == 'YYYY/dd/MM' || format == 'YYYY-dd-MM') {
                date = date[0] + separator + date[1] + separator + date[0];
            }
        }
        return new Date(date);
    }

    datetime.replace = function (date, format) {
        format = format || formatDate;
        date = Anb.datetime.standar(date, format);
        var monthLiteral = format.indexOf('MMM') != -1;
        return Anb.array.replace(format, [
            "dddd", "ddd", "dd", monthLiteral ? 'MMM' : 'MM', 'YYYY', 'HH', 'mm', 'ss'
        ], [
            dayslong[date.getDay()],
            days[date.getDay()],
            (date.getDate() < 10 ? '0' : '') + date.getDate(), 
            monthLiteral ? months[date.getMonth()] : ((date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1)), 
            date.getFullYear(), 
            (date.getHours() < 10 ? '0' : '') + date.getHours(), 
            (date.getMinutes() < 10 ? '0' : '') + date.getMinutes(), 
            (date.getSeconds() < 10 ? '0' : '') + date.getSeconds()
        ]);
    }

    datetime.last = function (date) {
        var now = new Date();
        var days = Anb.datetime.daysBetweenTwoDates(date, now);
        if (days > 31) {
            return Anb.datetime.dateLiteral(date);
        } else {
            if (days > 7) {
                var weeks = parseInt(days / 7);
                return 'Hace ' + weeks + ' semana' + (weeks > 1 ? 's' : '');
            } else {
                if (days >= 1) {
                    return Anb.datetime.format(date, 'ddd a la(s) HH:mm');
                } else {
                    var hours = Anb.datetime.hoursBetweenTwoDates(date, now);
                    if (Anb.datetime.isYesterday(date, hours)) {
                        return 'Ayer a la(s) ' + Anb.datetime.timeLiteral(date);
                    } else {
                        if (hours >= 1) {
                            return 'Hace ' + hours + ' hora' + (hours > 1 ? 's' : '');
                        } else {
                            var minutes = Anb.datetime.minutesBetweenTwoDates(date, now);
                            if (minutes >= 1) {
                                return 'Hace ' + minutes + ' minuto' + (minutes > 1 ? 's' : ''); 
                            } else {
                                var seconds = Anb.datetime.secondsBetweenTwoDates(date, now);
                                return 'Hace ' + seconds + ' segundo' + (seconds > 1 ? 's' : '');
                            }
                        }
                    }
                }
            }        
        }
    }

    datetime.secondsBetweenTwoDates = function (date1, date2, absolute) {
        return Anb.datetime.betweenTwoDates(date1, date2, "s", absolute);
    }

    datetime.minutesBetweenTwoDates = function (date1, date2, absolute) {
        return Anb.datetime.betweenTwoDates(date1, date2, "i", absolute);
    }

    datetime.hoursBetweenTwoDates = function (date1, date2, absolute) {
        return Anb.datetime.betweenTwoDates(date1, date2, "h", absolute);
    }

    datetime.daysBetweenTwoDates = function (date1, date2, absolute) {
        return Anb.datetime.betweenTwoDates(date1, date2, "d", absolute);
    }

    datetime.betweenTwoDates = function (date1, date2, type, absolute) {
        var types = {s : 1000, i : 60*1000, h : 60*60*1000, d : 24*60*60*1000};
        var diff = parseInt((Anb.datetime.standar(date2).getTime() - Anb.datetime.standar(date1).getTime())/types[type]);
        
        if (typeof absolute !== 'undefined' && absolute !== false) {
            return Math.abs(diff);
        }
        return diff;
    }
    
    datetime.isYesterday = function (date, hours) {
        if (Anb.datetime.isDate(date)) {
            date = date.split(' ');
            var time = date[1].split(':');
            return hours >= parseInt(time[0]);    
        }
        return false;
    }

    return datetime;

}());

Anb.load = (function () {

    function init() {
        var $buttonCollapse = $('.toggle-menu');
        var $asideActive = $('#sidebar .active').parent().parent();
        var $leftPanel = $('#sidebar');
        var mobile = $(window).width() < 768;

        render('#container');

        if($asideActive.hasClass('has-submenu')) {
            $asideActive.addClass('active');
        }
        
        $buttonCollapse.on('click', function() {
            $leftPanel.toggleClass('collapsed');
            var collapse = $leftPanel.hasClass('collapsed');
            $('header, footer').toggleClass('collapsed');
            if (!mobile) {
                $('.has-submenu.active > ul')[collapse ? 'hide' : 'show']();
                localStorage.setItem('anb-navbar', collapse ? 'yes' : 'no');
            } else {
                $('.has-submenu.active > ul').show();
            }
            if (collapse) {
                $('.has-submenu .list-unstyled').hide();
            }
        });

        if (!mobile && localStorage.getItem('anb-navbar') == 'yes') {
            $leftPanel.addClass('collapsed');
            $('header, footer').addClass('collapsed');
        }
        
        $leftPanel.find(".navigation > ul > li:has(ul) > a").on('click', function() {
            
            if( $leftPanel.hasClass('collapsed') == false || $(window).width() < 768 ) {
                $leftPanel.find(".navigation > ul > li > ul").slideUp(300);
                $leftPanel.find(".navigation > ul > li").removeClass('active');
            
                if(!$(this).next().is(":visible")) {
                    $(this).next().slideToggle(300);
                    $(this).closest('li').addClass('active');
                }
            
                return false;
            }   
        });

        $leftPanel.find(".navigation li ul").each(function () {
            var $this = $(this);
            if ($this.find('li').length == 0) {
                $this.remove();
            }
        });
        
        var $document = $(document),
            $container = $('#container'),
            $btnBanner = $('#btn-banner'),
            $btnFS = $('#btn-fullscreen'),
            $btnScroll = $('#btn-scroll-top');
            
        $btnFS.on('click', function (e) {
            e.preventDefault();
            Anb.fullscreen();
            $btnFS.toggleClass('active');
            $btnFS.tooltip('destroy').prop('title', $btnFS.hasClass('active') ? 'Salir pantalla completa' : 'Pantalla completa');
            setTimeout(function () {$btnFS.tooltip()}, 300);
        });
        
        var exitFullScreen = function () {
            $btnFS.removeClass('active');
            $btnFS.tooltip('destroy').prop('title', 'Pantalla completa');
            setTimeout(function () {$btnFS.tooltip()}, 300);
        }

        $document.on('scroll', function () {
            $btnScroll[$document.scrollTop() > 200 ? 'fadeIn' : 'fadeOut']();
        });
        
        $btnScroll.on('click', function(e) {
            e.preventDefault();
            $("html, body").animate({ scrollTop: 0 }, 800);
        });
        
        $document.on('keyup', function(e) {
            if (e.keyCode == 27) {
                exitFullScreen();
            }
        });
        
        document.addEventListener("mozfullscreenchange", function () {
            if (!document.mozFullScreen) {
                exitFullScreen();
            }
        }, false);
                
        if (localStorage.getItem('anb-banner') == 'yes' || localStorage.getItem('anb-banner') == null) {
            $btnBanner.addClass('active');
        } else {
            $btnBanner.tooltip('destroy').prop('title', 'Mostrar banner');
            setTimeout(function () {$btnBanner.tooltip()}, 300);
            $container.addClass('logo-off');
            $('#anb-messages').css({top: 50})
        }
        
        $btnBanner.on('click', function (e) {
            e.preventDefault();
            var active = $btnBanner.hasClass('active');
            $container.toggleClass('logo-off');
            $btnBanner.toggleClass('active');
            $btnBanner.tooltip('destroy').prop('title', active ? 'Mostrar banner' : 'Ocultar banner');
            setTimeout(function () {$btnBanner.tooltip()}, 300);
            localStorage.setItem('anb-banner', active ? 'no' : 'yes');
            $('#anb-messages').css({top: active ? 125 : 50});
        });

        if (Anb.ie > 0 && Anb.ie < 10) {
            alert("Usted tiene la versión " + Anb.ie + " de Internet Explorer, el sistema no es compatible con esta versión, por favor use Chrome o Firefox, o una versión mayor o igual a Internet Explorer 10.");
            throw new Error('Navegador no soportado');
        }
    }

    var render = function (container) {
        $(container + ' .required').each(function () {
            var $el = $(this),
                $label = $el.prev('label');
            if ($label.length == 0) {
                $label = $el.parent().prev('label');
            }
            $label.addClass('label-required');
        });
        Anb.form.integer(container + ' .integer');
        Anb.form.decimal(container + ' .decimal');
        Anb.form.maxlength(container);
        Anb.datetime.date(container + ' .datepicker');
        Anb.datetime.dateRange(container + ' .input-daterange');
        $(container + ' [data-toggle="tooltip"]').tooltip();
    }

    init(); // begin load

    var load = {};

    load.render = render;

    return load;

}());