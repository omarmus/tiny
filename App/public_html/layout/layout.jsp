<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<tiles:insert attribute="header" />
    <body>
        <div id="loading-ajax"></div>
        <div id="container">
            <tiles:insert attribute="navmain" />
            <div class="container-fluid" id="container-main">
                <tiles:insert attribute="content" />
            </div>
        </div>
        <tiles:insert attribute="footer" />

        <script src="lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="lib/datepicker/js/bootstrap-datepicker.js"></script>
        <script src="lib/datatables/js/jquery.dataTables.min.js"></script>
        <script src="lib/magicsuggest/magicsuggest.min.js"></script>
        <script src="lib/summernote/summernote.min.js"></script>
        
        <c:if test="${!empty PDF}">
            <script src="lib/jspdf/jspdf.js"></script>
            <script src="lib/jspdf/jspdf.plugin.ie_below_9_shim.js"></script>
            <script src="lib/jspdf/FileSaver.min.js"></script>
            <script src="lib/jspdf/jspdf.plugin.addimage.js"></script>
            <script src="lib/jspdf/BlobBuilder.js"></script>
            <script src="lib/jspdf/jspdf.plugin.standard_fonts_metrics.js"></script>
            <script src="lib/jspdf/jspdf.plugin.split_text_to_size.js"></script>
            <script src="lib/jspdf/jspdf.plugin.from_html.js"></script>
        </c:if>
    
        <script src="js/main.js"></script>
        
        <%@ include file="/notifica/index.jsp" %>
    </body>
</html>
