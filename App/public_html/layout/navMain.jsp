<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<c:set var="list" value="envelope-square file-text-o cog list-alt cog paperclip"/>
<c:set var="icons" value="${fn:split(list, ' ')}" />
<c:set var="i" value="0" />

<header>
    <div class="logo" id="logo"><h1>Courier</h1></div>
    <div class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header pull-left">
                <button class="navbar-toggle pull-left visible-xs toggle-menu" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button class="btn btn-default toggle-menu hidden-xs" type="button">
                    <span class="hide-menu"><i class="glyphicon glyphicon-menu-left"></i> <span>Ocultar menú</span></span>
                    <span class="show-menu"><span>Mostrar menú</span> <i class="glyphicon glyphicon-menu-right"></i></span>
                </button>
            </div>
            <div class="pull-right">
                <ul class="breadcrumb pull-left">
                    <c:if test="${!empty parentURI}">
                        <li>${parentURI}</li>
                    </c:if>
                    <li class="active">${URI}</li>
                </ul>
                <ul class="header-menu nav navbar-nav pull-left pull-left">
                    <li>
                        <a id="btn-banner" class="btn-banner hidden-xs" href="#" data-toggle="tooltip" data-placement="bottom" title="Ocultar banner">
                            <i class="fa fa-photo"></i>
                        </a>
                    </li>
                    <li>
                        <a id="btn-fullscreen" class="btn-fullscreen hidden-xs" href="#" data-toggle="tooltip" data-placement="bottom" title="Pantalla completa">
                            <i class="fa fa-expand"></i>
                            <i class="fa fa-compress"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="not-open" data-toggle="tooltip" data-placement="bottom" title="Notificaciones">
                            <i class="fa fa-bell-o"></i>
                            <span class="badge badge-danger badge-header" id="not-number-state"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div><!-- /.container-fluid -->
    </div>
</header>
<nav id="sidebar">
    <div class="navigation">
        <div class="user-data">
            <div class="user-icon pull-left">
                <i class="fa fa-user"></i>
            </div>
            <h4 class="user-details pull-left">
                ${sessionScope.usuario.nombreUsuario}<br>
                <sub id="administracion">
                    <i class="fa fa-plane"></i> Aeropuerto ${sessionScope.aduana == '211' ? 'El Alto' : sessionScope.aduana == '311' ? 'Cochabamba' : 'Viru-Viru'}
                </sub>
            </h4>
        </div>
        <div class="menu-title">
            Menú principal
        </div>
        <ul class="list-unstyled">
            <li class="${ACCION == 'dashboard' ? 'active' : ''}">
                <a href="dashboard.do">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <c:set var="open" value="false" />
            <c:forEach items="${sessionScope.opciones}" var="opcion">
                <c:choose>
                    <c:when test="${opcion.codopc != '' && opcion.codant == '0'}">
                        <c:if test="${open}">
                            </ul>
                        </li>
                        </c:if>
                        <c:set var="open" value="true" />
                        <li class="has-submenu">
                            <a href="#">
                                <i class="fa fa-${icons[i]}"></i> 
                                <span class="nav-label">${opcion.desc}</span>
                                <i class="fa fa-chevron-right pull-right"></i>
                            </a>
                            <ul class="list-unstyled">
                            <c:set var="i" value="${i+1}" />
                    </c:when>
                    <c:otherwise>
                        <c:if test="${ opcion.codant != 2 }">
                            <li class="${ACCION == opcion.accion ? 'active' : ''}">
                                <a href="${opcion.accion}.do">${opcion.desc}</a>
                            </li>
                        </c:if>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <c:if test="${open}">
                    </ul>
                </li>
            </c:if>  
            <li>
                <a href="logout.do">
                    <i class="fa fa-power-off"></i>
                    <span class="nav-label">Cerrar sesión</span>
                </a>
            </li>
        </ul>
    </div>
</nav>