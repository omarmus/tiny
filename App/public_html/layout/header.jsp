<%@ page contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <title>ANB ${ !empty parentURI ? ' - ' : '' } ${parentURI} ${ !empty URI ? ' - ' : '' } ${URI}</title>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
    
        <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="lib/datepicker/css/datepicker.css">
        <link rel="stylesheet" type="text/css" href="lib/magicsuggest/magicsuggest.min.css">
        <link rel="stylesheet" type="text/css" href="lib/summernote/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="lib/summernote/summernote.css">
        <link rel="stylesheet" type="text/css" href="lib/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">

        <script src="lib/jquery/jquery-1.11.2.min.js"></script>  
        <script type="text/javascript">
            $(document).ready(function() {
                <c:if test="${!empty OK}">
                    Anb.message.ok('${OK}');
                </c:if>
                <c:if test="${!empty ERROR}">
                    Anb.message.error('${ERROR}');
                </c:if>
                <c:if test="${!empty EMAIL}">
                    Anb.message.email('${EMAIL}');
                </c:if>
                <c:if test="${!empty NOTIFICATION}">
                    Anb.message.notification('${NOTIFICATION}');
                </c:if>
            });
        </script>       
    </head>
