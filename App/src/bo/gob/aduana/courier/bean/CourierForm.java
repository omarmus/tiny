package bo.gob.aduana.courier.bean;

import org.apache.struts.action.ActionForm;

/**
 * 
 * @author Omar Gutiérrez Condori <ogutierrez@aduana.gob.bo>
 */
public class CourierForm extends ActionForm {

    // Primary Key: key_cuo (aduana), key_voy_nber (nro_viaje), key_dep_date (key_dep_date), key_bol_ref (doc_embarque)
    private String aduana; //key_cuo
    private String nro_viaje; //key_voy_nber hidden                                                                                   
    private String fecha_partida; //key_dep_date fecha de registro hidden
    private String doc_embarque; //key_bol_ref
    private String nro_linea; // key_lin_nbr hidden
    private String anio_manif; //car_reg_year
    private int nro_manif; //car_reg_nber
    private String fecha_manif; //car_reg_date
    private String doc_consignatario; // carbol_cons_cod
    private String consignatario; //carbol_cons_nam    
    private String tipo; //cou_tipo_guia
    private int cantidad; //car_pkg_avl
    private double peso; //carbol_gros_mas - cuo_peso_manif
    private double peso_real; //car_wgt_avl - cuo_peso_real
    private double peso_observado; //car_wgt_avl - cuo_peso_obs
    private String observacion; //cou_observacion
    private String descripcion; //cou_descripcion
    private String usuario;
    private String estado; //cou_estado
    
    public int getNro_manif() {
        return nro_manif;
    }

    public void setNro_manif(int nro_manif) {
        this.nro_manif = nro_manif;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getAnio_manif() {
        return anio_manif;
    }

    public void setAnio_manif(String anio_manif) {
        this.anio_manif = anio_manif;
    }

    public String getFecha_manif() {
        return fecha_manif;
    }

    public void setFecha_manif(String fecha_manif) {
        this.fecha_manif = fecha_manif;
    }

    public double getPeso_observado() {
        return peso_observado;
    }

    public void setPeso_observado(double peso_observado) {
        this.peso_observado = peso_observado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getAduana() {
        return aduana;
    }

    public void setAduana(String aduana) {
        this.aduana = aduana;
    }

    public String getNro_viaje() {
        return nro_viaje;
    }

    public void setNro_viaje(String nro_viaje) {
        this.nro_viaje = nro_viaje;
    }

    public String getFecha_partida() {
        return fecha_partida;
    }

    public void setFecha_partida(String fecha_partida) {
        this.fecha_partida = fecha_partida;
    }

    public String getDoc_embarque() {
        return doc_embarque;
    }

    public void setDoc_embarque(String doc_embarque) {
        this.doc_embarque = doc_embarque;
    }

    public String getNro_linea() {
        return nro_linea;
    }

    public void setNro_linea(String nro_linea) {
        this.nro_linea = nro_linea;
    }

    public String getDoc_consignatario() {
        return doc_consignatario;
    }

    public void setDoc_consignatario(String doc_consignatario) {
        this.doc_consignatario = doc_consignatario;
    }

    public String getConsignatario() {
        return consignatario;
    }

    public void setConsignatario(String consignatario) {
        this.consignatario = consignatario;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPeso_real() {
        return peso_real;
    }

    public void setPeso_real(double peso_real) {
        this.peso_real = peso_real;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
