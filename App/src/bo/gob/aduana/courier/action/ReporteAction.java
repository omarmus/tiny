package bo.gob.aduana.courier.action;

import bo.gob.aduana.courier.ent.Courier;
import bo.gob.aduana.courier.neg.CourierNeg;

import bo.gob.aduana.general.ent.Respuesta;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.MappingDispatchAction;

public class ReporteAction extends MappingDispatchAction {
    
    private final CourierNeg courier = new CourierNeg();
    
    public ActionForward autorizacion(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {
                
        return mapping.findForward("reporte.autorizacion");
    }

    public ActionForward retenido(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {
                
        return mapping.findForward("reporte.retenido");
    }
    
    public ActionForward reporte(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {
                
        String aduana = (String) request.getSession().getAttribute("aduana");
        String estado = request.getParameter("estado");

        String nro_manif = request.getParameter("nro_manif");
        String doc_embarque = request.getParameter("doc_embarque");
        String fecha_manif = request.getParameter("fecha_manif");
        String usuario = request.getParameter("usuario"); 
        String tipo = request.getParameter("tipo"); 
        
        if (estado.equals("PENDIENTE")) {
            Respuesta<Courier[]> res = courier.reportePendientes(aduana, nro_manif, doc_embarque, fecha_manif, usuario, tipo);
            if (res.getCodigo() == 1) {
                request.setAttribute("couriers", res.getResultado());    
            } else {
                request.setAttribute("ERROR", res.getMensaje());
            }
        } else {
            Respuesta<Courier[]> res = courier.reporte(aduana, nro_manif, doc_embarque, fecha_manif, usuario, tipo, estado);
            if (res.getCodigo() == 1) {
                request.setAttribute("couriers", res.getResultado());    
            } else {
                request.setAttribute("ERROR", res.getMensaje());
            }
        }
        
        return mapping.findForward("reporte.lista");
    }
}
