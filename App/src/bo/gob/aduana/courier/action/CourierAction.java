package bo.gob.aduana.courier.action;

import bo.gob.aduana.courier.bean.CourierForm;
import bo.gob.aduana.courier.ent.Courier;
import bo.gob.aduana.courier.ent.Descargo;
import bo.gob.aduana.courier.neg.CourierNeg;
import bo.gob.aduana.general.ent.Option;
import bo.gob.aduana.general.ent.Respuesta;
import bo.gob.aduana.system.Json;
import bo.gob.aduana.system.Util;

import com.google.gson.Gson;

import java.util.ArrayList;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.MappingDispatchAction;

/**
 * 
 * @author 
 */
public class CourierAction extends MappingDispatchAction {
    
    private final CourierNeg courier = new CourierNeg();
    
    public ActionForward index(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {

        Util.noCache(response);
        
        String aduana = (String) request.getSession().getAttribute("aduana");
        
        Respuesta<Courier[]> res = courier.lista(aduana);
        if (res.getCodigo() == 1) {
            request.setAttribute("couriers", res.getResultado());
        } else {
            request.setAttribute("ERROR", res.getMensaje());
        }
        
        return mapping.findForward("courier.index");
    }    
    
    public ActionForward guiasManifiesto(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {

        Util.isAjax(request, response);
        String query = request.getParameter("query");
        String gestion = request.getParameter("gestion");
        String aduana = request.getParameter("aduana");
        String manifiesto = request.getParameter("manifiesto"); 
        
        Respuesta<Courier[]> res = courier.guias(query, gestion, aduana, manifiesto);
                       
        if (res.getCodigo() == 1) {
            List options = new ArrayList();
            Courier[] results = res.getResultado();
            for (int i = 0; i < results.length; i++) {
                options.add(new Option(results[i].getDoc_embarque(), String.valueOf(results[i].getPeso())));
            }
            Json.print(response, new Gson().toJson(options));
        } else {
            Json.error(response, res.getMensaje());
        }
            
        return null;
    }

    public ActionForward detalles(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {
                
        
        String fecha_partida = request.getParameter("fecha-partida");
        String aduana = request.getParameter("aduana");
        String nro_viaje = request.getParameter("nro-viaje");
        String doc_embarque = request.getParameter("doc-embarque");

        Respuesta<Courier> res = courier.getCourier(fecha_partida, aduana, nro_viaje, doc_embarque);
        if (res.getCodigo() == 1) {
            Courier c = res.getResultado();
            request.setAttribute("CourierForm", c);
            if (request.getParameter("view") != null) {
                request.setAttribute("view", "YES");
                Respuesta<Descargo[]> des = courier.getDescargos(fecha_partida, aduana, nro_viaje, doc_embarque);
                if (des.getCodigo() == 1) {
                    request.setAttribute("descargos", des.getResultado());
                    return mapping.findForward("courier.detalles");
                } else {
                    request.setAttribute("ERROR", des.getMensaje());
                    Json.error(response, des.getMensaje());
                    return null;
                }
            }
            String view = c.getEstado().equals("RETENIDO") ? "levantar" : c.getEstado().equals("POR AUTORIZAR") ? "autorizar" : "revisar";
            return mapping.findForward("courier." + view);
        } else {
            Json.error(response, res.getMensaje());
        }
        return null;
    }

    public ActionForward retencion(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {
                
        String fecha_partida = request.getParameter("fecha-partida");
        String aduana = request.getParameter("aduana");
        String nro_viaje = request.getParameter("nro-viaje");
        String doc_embarque = request.getParameter("doc-embarque");

        Respuesta<Courier> res = courier.getCourier(fecha_partida, aduana, nro_viaje, doc_embarque);
        if (res.getCodigo() == 1) {
            request.setAttribute("CourierForm", res.getResultado());
            return mapping.findForward("courier.retencion");
        }
        Json.error(response, res.getMensaje());
        return null;
    }

    public ActionForward guardarRetencion(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {

        Util.isAjax(request, response);

        CourierForm c = (CourierForm)form;
        c.setUsuario((String) request.getSession().getAttribute("USER"));
        c.setEstado("RETENIDO");
        
        Respuesta<Boolean> res = courier.guardar(new Courier());
        
        if(res.getCodigo() == 1) {
            Json.message(response, "CREATE-AJAX", "La retensión fué guardada correctamente.");            
        } else {
            Json.error(response, res.getMensaje());
        }
        return null;
    }

    public ActionForward guardarLevantar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {

        Util.isAjax(request, response);

        CourierForm c = (CourierForm)form;
        c.setUsuario((String) request.getSession().getAttribute("USER"));
        c.setEstado("POR AUTORIZAR");
        Respuesta<Boolean> res = courier.guardar(new Courier());
        
        if(res.getCodigo() == 1) {
            Json.message(response, "CREATE-AJAX", "La retensión fué guardada correctamente.");            
        } else {
            Json.error(response, res.getMensaje());
        }
        return null;
    }

    public ActionForward autorizar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {
                
        String fecha_partida = request.getParameter("fecha-partida");
        String aduana = request.getParameter("aduana");
        String nro_viaje = request.getParameter("nro-viaje");
        String doc_embarque = request.getParameter("doc-embarque");

        Respuesta<Courier> res = courier.getCourier(fecha_partida, aduana, nro_viaje, doc_embarque);
        if (res.getCodigo() == 1) {
            request.setAttribute("CourierForm", res.getResultado());
            return mapping.findForward("courier.retencion");
        }
        Json.error(response, res.getMensaje());
        
        return null;
    }

    public ActionForward guardarAutorizar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {

        Util.isAjax(request, response);

        CourierForm c = (CourierForm)form;
        c.setUsuario((String) request.getSession().getAttribute("USER"));
        c.setEstado("AUTORIZADO");
        Respuesta<Boolean> res = courier.guardar(new Courier());
        
        if(res.getCodigo() == 1) {
            Json.message(response, "CREATE-AJAX", "La retensión fué guardada correctamente.");            
        } else {
            Json.error(response, res.getMensaje());
        }
        return null;
    }
    
    public ActionForward pesoReal(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {

        Util.isAjax(request, response);
        
        Respuesta<String> res = courier.pesoReal(new Courier());
        
        if(res.getCodigo() == 1) {
            response.getWriter().write(res.getResultado());            
        } else {
            Json.error(response, res.getMensaje());
        }
        
        return null;
    }
    
    // Impresión
    public ActionForward impresion(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {
                
        request.setAttribute("PDF", true); //Cargando libreria JS-PDF
        
        return mapping.findForward("courier.impresion");
    }
    
    public ActionForward guiaImpresion(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {
        
        String gestion = request.getParameter("gestion");
        String aduana = request.getParameter("aduana");
        String manifiesto = request.getParameter("manifiesto");
        String doc_embarque = request.getParameter("doc_embarque");

        Respuesta<Courier> res = courier.getGuiaImpresion(gestion, aduana, manifiesto, doc_embarque);
        
        if (res.getCodigo() == 1) {
            request.setAttribute("CourierForm", res.getResultado());
            return mapping.findForward("courier.guia");
        }
        Json.error(response, res.getMensaje());
        return null;
    }
    
    public ActionForward imprimirGuia(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {

        Util.isAjax(request, response);

        CourierForm c = (CourierForm)form;
        c.setUsuario((String) request.getSession().getAttribute("USER"));
        c.setEstado("IMPRIMIDO");
        c.setObservacion("Con constancia de entrega");
        
        Respuesta<Boolean> res = courier.guardar(new Courier());
        
        if(res.getCodigo() == 1) {
            Json.success(response, "Se registró la Constancia de Entrega de la Guía Courier correctamente.");            
        } else {
            Json.error(response, res.getMensaje());
        }

        return null;
    }
}
