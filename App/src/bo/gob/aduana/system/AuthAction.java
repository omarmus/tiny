package bo.gob.aduana.system;


import bo.gob.aduana.general.dao.Dao;
import bo.gob.aduana.lib.log.Log;
import bo.gob.aduana.lib.notifica.Notifica;

import cliente.ClaseEnvio;
import cliente.ServiciosUsuario;

import cliente.bean.ClaseOpcion;

import java.io.IOException;
import java.io.StringReader;

import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.MappingDispatchAction;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;


/*
*   Nombre de la clase: AuthAction, Clase de autentificación del usuario
*
*   Fecha creación, Fecha Modificación
*
*   Autor creador, Autor Modificador
*/

public class AuthAction extends MappingDispatchAction {
       
    // Nombre del sistema en GUSUARIO
    private final String APP = "COURIER";
    
    // Ruta del paquete base para el Log del sistema
    private final String PKG_BASE = "bo.gob.aduana.courier";
    
    // JDBC del sistema
    private final String JDBC = "jdbc/basy";
    
    protected ActionForward loginUser(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response, boolean ajax) throws IOException, ServletException {

        // Datos para el Log
        Log.APP = APP;
        Log.PKG_BASE = PKG_BASE;
        Log.IP = request.getRemoteHost();
        
        AuthForm usuario = (AuthForm)form;
        ActionMessages errors = new ActionMessages();

        ServiciosUsuario serviciosUsuario = new ServiciosUsuario();
        ClaseEnvio claseEnvio = serviciosUsuario.getServiciosUsuario();

        try {
            // Inicio Autentificacion Virtual
            String xmlv = "";
            String vparam = request.getParameter("vsessionid");
            DocumentBuilderFactory factoryv = DocumentBuilderFactory.newInstance();
            DocumentBuilder builderv = factoryv.newDocumentBuilder();
            if (vparam != null) {
                try {
                    xmlv = claseEnvio.fUsuarioVirtual(vparam);
                    Document doc = builderv.parse(new InputSource(new StringReader(xmlv)));
                    doc.getDocumentElement().normalize();
                    usuario.setUsuario(getTagXML(doc, "Usuario"));
                    usuario.setClave(getTagXML(doc, "Clave"));
                } catch (Exception ex) {
                    ;
                }
            }
           
            String xml = claseEnvio.listaOpcionesXML(usuario.getUsuario().toUpperCase(), usuario.getClave(), APP);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xml)));
            doc.getDocumentElement().normalize();

            ClaseSession cs = new ClaseSession();
            cs.setUsuario(usuario.getUsuario().toUpperCase());
            cs.setNombreUsuario(Util.capitalizeString(getTagXML(doc, "Usuario")));
            cs.setPerfil(getTagXML(doc, "Perfil"));
            cs.setNit(getTagXML(doc, "Nit"));
            cs.setNomGerencia(getTagXML(doc, "NombreGerencia"));
            cs.setNomUnidad(getTagXML(doc, "NombreUnidad"));
            cs.setCorreo(getTagXML(doc, "Correo"));
            cs.setAduana(getTagXML(doc, "Aduana"));

            Calendar cal = Calendar.getInstance();
            cs.setGestion((cal.get(Calendar.YEAR)));

            usuario.getOpciones().clear();
            usuario.setOpciones(new ArrayList());
            NodeList OpcionesLista =
                ((Element)doc.getElementsByTagName("Opciones").item(0)).getElementsByTagName("Opcion");
            
            if (OpcionesLista != null) {
                for (int i = 0; i < OpcionesLista.getLength(); i++) {
                    Element itemOpcion = (Element)OpcionesLista.item(i);
                    ClaseOpcion bean = new ClaseOpcion();
                    bean.setCodopc(getTagXML(itemOpcion, "Codigo"));
                    bean.setCodant(getTagXML(itemOpcion, "NivelSuperior"));
                    bean.setAccion(getTagXML(itemOpcion, "Accion"));
                    String name = getTagXML(itemOpcion, "Descripcion");
                    bean.setDesc(bean.getCodant().equals("0") ? Util.capitalizeString(name) : name);
                    usuario.getOpciones().add(bean);
                }
            }
            
            request.setAttribute("URI", "Dashboard");
            request.getSession().setAttribute("opciones", usuario.getOpciones());
            request.getSession().setAttribute("usuario", cs);
            request.getSession().setAttribute("aduana", usuario.getAduana());
            
            // Datos para el sistema de Notificaciones
            String USER = usuario.getUsuario().toUpperCase();
            
            request.getSession().setAttribute("USER", USER);
            request.getSession().setAttribute("APP", APP);
            request.getSession().setAttribute("TOKEN", Notifica.saveToken(USER, APP, request.getLocalAddr(), request.getHeader("User-Agent")));
            // Fin de los datos para Notificaciones
            
            // JDBC para el módulo de persistencia
            Dao.JDBC = JDBC;
            
            // Datos para el Log
            Log.USUARIO = cs.getUsuario();
            Log.info("El usuario '" + Log.USUARIO + "' ingreso al sistema", "LOGIN");
            
        } catch (Exception e) {
            errors.add("message", new ActionMessage("message.error", "El usuario y/o contraseña no son válidos."));
            saveErrors(request, errors);
            
            Log.USUARIO = usuario.getUsuario();
            Log.error("Error de logeo", "LOGIN", e);
            return mapping.findForward("index");
        }
        
        if (ajax) {
            response.getWriter().write("OK");
            return null;
        } else {
            return mapping.findForward("dashboard.index");
        }
            
    }
    
    public ActionForward index(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws IOException, ServletException {
        
        return mapping.findForward("index");
    }
    
    public ActionForward login(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws IOException, ServletException {
        
        return loginUser(mapping, form, request, response, false);
    }
    
    public ActionForward loginAjaxForm(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws IOException, ServletException {
        
        return mapping.findForward("usuario.loginAjax");
    }
    
    public ActionForward loginAjax(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws IOException, ServletException {

        return loginUser(mapping, form, request, response, true);
    }
    
    public ActionForward logout(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) throws IOException, ServletException {
        
        request.getSession().removeAttribute("APP");
        request.getSession().removeAttribute("USER");
        request.getSession().removeAttribute("TOKEN");
        request.getSession().removeAttribute("usuario");
        request.getSession().removeAttribute("opciones");
        
        return mapping.findForward("index");
    }
    
    private String getTagXML(Document doc, String tag) {
        try {
            NodeList listaNodosHijos = doc.getElementsByTagName(tag);
            return listaNodosHijos.item(0).getFirstChild().getNodeValue();
        } catch (Exception e) {
            return "";
        }
    }

    private String getTagXML(Element doc, String tag) {
        try {
            return (doc.getElementsByTagName(tag).item(0).getFirstChild().getNodeValue());
        } catch (Exception e) {
            return "";
        }
    }
}
