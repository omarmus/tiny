package bo.gob.aduana.system;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionMessage;

public class AuthForm extends ActionForm {

    private String usuario;
    private String clave;
    private String aduana;
    private ArrayList opciones = new ArrayList();

    public String getAduana() {
        return aduana;
    }

    public void setAduana(String aduana) {
        this.aduana = aduana;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public ArrayList getOpciones() {
        return opciones;
    }

    public void setOpciones(ArrayList opciones) {
        this.opciones = opciones;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        
        ActionErrors errors = new ActionErrors();
        
        if (usuario == null || usuario.equals("")) {
            errors.add("usuario", new ActionMessage("error.required", "Usuario"));
        }
        
        if (clave == null || clave.equals("")) {
            errors.add("clave", new ActionMessage("error.required", "Contraseña"));
        }
        
        if (aduana == null || aduana.equals("-")) {
            errors.add("aduana", new ActionMessage("error.required", "Aduana"));
        }
        
        return errors;
    }
}
