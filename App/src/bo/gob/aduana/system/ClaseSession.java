package bo.gob.aduana.system;

/*   
*   Nombre de la clase: ClaseSession, variables de sesion para el sistema
*
*   Fecha creación, Fecha Modificación
*
*   Autor creador, Autor Modificador
*/
 
public class ClaseSession {   
    private String usuario = "";
    private String perfil = "";
    private String nit = "";    
    private String aduana = ""; 
    private String nombreUsuario;
    private String area="";        
    private String varAux="";
    private String nomGerencia;
    private String nomUnidad;
    private String correo;
    private int gestion=0;
    
    public ClaseSession() { 
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getAduana() {
        return aduana;
    }

    public void setAduana(String aduana) {
        this.aduana = aduana;
    }


    public void setArea(String area) {
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public void setGestion(int gestion) {
        this.gestion = gestion;
    }

    public int getGestion() {
        return gestion;
    }

    public void setVarAux(String varAux) {
        this.varAux = varAux;
    }

    public String getVarAux() {
        return varAux;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNomGerencia(String nomGerencia) {
        this.nomGerencia = nomGerencia;
    }

    public String getNomGerencia() {
        return nomGerencia;
    }

    public void setNomUnidad(String nomUnidad) {
        this.nomUnidad = nomUnidad;
    }

    public String getNomUnidad() {
        return nomUnidad;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCorreo() {
        return correo;
    }
}
