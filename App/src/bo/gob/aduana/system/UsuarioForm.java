package bo.gob.aduana.system;

import org.apache.struts.action.ActionForm;

public class UsuarioForm extends ActionForm {
    private String usuario = "";
    private String clave = "";
    private String nuevo = "";
    private String confnuevo = "";
    private int opcion = 0;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNuevo() {
        return nuevo;
    }

    public void setNuevo(String nuevo) {
        this.nuevo = nuevo;
    }

    public String getConfnuevo() {
        return confnuevo;
    }

    public void setConfnuevo(String confnuevo) {
        this.confnuevo = confnuevo;
    }

    public int getOpcion() {
        return opcion;
    }

    public void setOpcion(int opcion) {
        this.opcion = opcion;
    }

}
