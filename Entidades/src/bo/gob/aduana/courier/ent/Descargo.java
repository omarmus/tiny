package bo.gob.aduana.courier.ent;

/**
 * 
 * @author Omar Gutiérrez Condori <ogutierrez@aduana.gob.bo>
 */
public class Descargo {
    
    String key_cuo;
    String key_voy_nber;
    String key_dep_date;
    String key_bol_ref;
    String reg_cuo;
    String reg_voy_nber;
    String reg_dep_date;
    String reg_bol_ref;
    double reg_peso;

    public String getKey_cuo() {
        return key_cuo;
    }

    public void setKey_cuo(String key_cuo) {
        this.key_cuo = key_cuo;
    }

    public String getKey_voy_nber() {
        return key_voy_nber;
    }

    public void setKey_voy_nber(String key_voy_nber) {
        this.key_voy_nber = key_voy_nber;
    }

    public String getKey_dep_date() {
        return key_dep_date;
    }

    public void setKey_dep_date(String key_dep_date) {
        this.key_dep_date = key_dep_date;
    }

    public String getKey_bol_ref() {
        return key_bol_ref;
    }

    public void setKey_bol_ref(String key_bol_ref) {
        this.key_bol_ref = key_bol_ref;
    }

    public String getReg_cuo() {
        return reg_cuo;
    }

    public void setReg_cuo(String reg_cuo) {
        this.reg_cuo = reg_cuo;
    }

    public String getReg_voy_nber() {
        return reg_voy_nber;
    }

    public void setReg_voy_nber(String reg_voy_nber) {
        this.reg_voy_nber = reg_voy_nber;
    }

    public String getReg_dep_date() {
        return reg_dep_date;
    }

    public void setReg_dep_date(String reg_dep_date) {
        this.reg_dep_date = reg_dep_date;
    }

    public String getReg_bol_ref() {
        return reg_bol_ref;
    }

    public void setReg_bol_ref(String reg_bol_ref) {
        this.reg_bol_ref = reg_bol_ref;
    }

    public double getReg_peso() {
        return reg_peso;
    }

    public void setReg_peso(double reg_peso) {
        this.reg_peso = reg_peso;
    }
    
    
}
