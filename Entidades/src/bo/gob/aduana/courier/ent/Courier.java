package bo.gob.aduana.courier.ent;

public class Courier {
    // Primary Key: key_cuo (aduana), key_voy_nber (nro_viaje), key_dep_date (key_dep_date), key_bol_ref (doc_embarque)
    private String aduana; //key_cuo
    private String nro_viaje; //key_voy_nber hidden                                                                                   
    private String fecha_partida; //key_dep_date fecha de registro hidden
    private String doc_embarque; //key_bol_ref
    private String nro_linea; // key_lin_nbr hidden
    private String anio_manif; //car_reg_year
    private int nro_manif; //car_reg_nber
    private String fecha_manif; //car_reg_date
    private String doc_consignatario; // carbol_cons_cod
    private String consignatario; //carbol_cons_nam    
    private String tipo; //cou_tipo_guia
    private int cantidad; //car_pkg_avl
    private double peso; //carbol_gros_mas - cuo_peso_manif
    private double peso_real; //car_wgt_avl - cuo_peso_real
    private double peso_observado; //car_wgt_avl - cuo_peso_obs
    private String observacion; //cou_observacion
    private String descripcion; //cou_descripcion
    private String usuario;
    private String estado; //cou_estado
    
    // Manifiesto de descargo
    private String des_gestion;
    private String des_aduana;
    private String des_manifiesto;
    private String documentos; //Documentos de embarque

    public void setAduana(String aduana) {
        this.aduana = aduana;
    }

    public String getAduana() {
        return aduana;
    }

    public void setNro_viaje(String nro_viaje) {
        this.nro_viaje = nro_viaje;
    }

    public String getNro_viaje() {
        return nro_viaje;
    }

    public void setFecha_partida(String fecha_partida) {
        this.fecha_partida = fecha_partida;
    }

    public String getFecha_partida() {
        return fecha_partida;
    }

    public void setDoc_embarque(String doc_embarque) {
        this.doc_embarque = doc_embarque;
    }

    public String getDoc_embarque() {
        return doc_embarque;
    }

    public void setNro_linea(String nro_linea) {
        this.nro_linea = nro_linea;
    }

    public String getNro_linea() {
        return nro_linea;
    }

    public void setAnio_manif(String anio_manif) {
        this.anio_manif = anio_manif;
    }

    public String getAnio_manif() {
        return anio_manif;
    }

    public void setNro_manif(int nro_manif) {
        this.nro_manif = nro_manif;
    }

    public int getNro_manif() {
        return nro_manif;
    }

    public void setFecha_manif(String fecha_manif) {
        this.fecha_manif = fecha_manif;
    }

    public String getFecha_manif() {
        return fecha_manif;
    }

    public void setDoc_consignatario(String doc_consignatario) {
        this.doc_consignatario = doc_consignatario;
    }

    public String getDoc_consignatario() {
        return doc_consignatario;
    }

    public void setConsignatario(String consignatario) {
        this.consignatario = consignatario;
    }

    public String getConsignatario() {
        return consignatario;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso_real(double peso_real) {
        this.peso_real = peso_real;
    }

    public double getPeso_real() {
        return peso_real;
    }

    public void setPeso_observado(double peso_observado) {
        this.peso_observado = peso_observado;
    }

    public double getPeso_observado() {
        return peso_observado;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    public void setDes_gestion(String des_gestion) {
        this.des_gestion = des_gestion;
    }

    public String getDes_gestion() {
        return des_gestion;
    }

    public void setDes_aduana(String des_aduana) {
        this.des_aduana = des_aduana;
    }

    public String getDes_aduana() {
        return des_aduana;
    }

    public void setDes_manifiesto(String des_manifiesto) {
        this.des_manifiesto = des_manifiesto;
    }

    public String getDes_manifiesto() {
        return des_manifiesto;
    }

    public void setDocumentos(String documentos) {
        this.documentos = documentos;
    }

    public String getDocumentos() {
        return documentos;
    }
}
