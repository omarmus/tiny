package bo.gob.aduana.general.ent;

public class Option {
    
    private String value;
    private String text;

    public Option(String value, String text) {
        this.value = value;
        this.text = text;
    }
    
    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
